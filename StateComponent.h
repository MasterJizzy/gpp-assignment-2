#pragma once
#include "Components.h"
#include "State.h"
#include <string>

class StateComponent : public Component {
private:
	int max_states_ = 1000;
	int state_count_ = 0;
	State* active_state;
	State* state_array_[1000];
	bool has_init = false;

public:
	StateComponent(State* state):state_array_()
	{
		addState(state);
		active_state = state;
	}

	~StateComponent()
	{

	}

	void init() override
	{
		//active_state->init();
	}

	void update(const float& frametime) override
	{
		if (has_init == false)
		{
			//active_state->init();
			has_init = true;
		}
		active_state->update();
		active_state->check();
	}

	void draw() override
	{

	}
	bool addState(State* state)
	{
		if (state_count_ < max_states_)
		{
			state_array_[state_count_] = state;
			state_count_++;
			return true;
		}
		else
		{
			return false;
		}

	}

	void changeState(std::string name)
	{
		has_init == false;
		for (int i=0;i<state_count_;i++)
		{
			if (state_array_[i]!= nullptr)
			{
				if (state_array_[i]->name == name)
				{
					active_state = state_array_[i];
					break;
				}
			}
		}
	}
};

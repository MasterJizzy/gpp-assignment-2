#include "Node.h"
int Connection::current_con_id;
Connection::Connection(float c, Node* f, Node* t)
	:
	id(current_con_id++),
	cost_(c),
	fromNode(f),
	toNode(t)
{

}
Connection::~Connection()
{

}
int Node::current_node_id;
Node::Node(VECTOR2 p)
	:
	id(current_node_id++),
	position_(p),
	connection_array_()
{
	active = true;
	current_connections_ = 0;
	max_connections_ = 10;
}
void Node::changeActive(bool a)
{
	active = a;
}
Node::~Node()
{

}
void Node::addConnection(Node* t, float c)
{
	if (current_connections_ < max_connections_) 
	{
		connection_array_[current_connections_] =  new Connection(c,this,t);
		current_connections_++;
	}
}
int NodeRecord::current_node_record_id;
NodeRecord::NodeRecord(Node* n, Connection* c, float cost, float e)
	:
	id(current_node_record_id++),
	node(n),
	con(c),
	costSoFar(cost)
{
	estimatedCost = e;
}

NodeRecord::~NodeRecord()
{

}


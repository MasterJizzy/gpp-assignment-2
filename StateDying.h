#pragma once
#include "Components.h"
#include "State.h"
#include "TwoTowers.h"
class StateDying : public State
{
private:
	Entity *target;
public:
	StateDying(Entity* o, std::string n)
		:
		State(o, n)
	{

	}
	~StateDying()
	{

	}
	void init() override
	{
		//add sprite change here
		if (owner_->hasGroup(TwoTowers::enemy))
		{
			if (owner_->hasGroup(TwoTowers::ranged))
			{
				if (owner_->hasGroup(TwoTowers::units))
				{
					//if ranged enemy unit
				}
				else if (owner_->hasGroup(TwoTowers::towers))
				{
					//if ranged enemy tower
				}
			}
			else if (owner_->hasGroup(TwoTowers::melee))
			{
				if (owner_->hasGroup(TwoTowers::units))
				{
					//if melee enemy unit
				}
				else if (owner_->hasGroup(TwoTowers::towers))
				{
					//if enemy wall
				}
			}
			else if (owner_->hasGroup(TwoTowers::base_tower))
			{
				//if enemy base
			}
		}
		else if (owner_->hasGroup(TwoTowers::friendly))
		{
			if (owner_->hasGroup(TwoTowers::ranged))
			{
				if (owner_->hasGroup(TwoTowers::units))
				{
					//if ranged friendly unit
				}
				else if (owner_->hasGroup(TwoTowers::towers))
				{

					//if ranged friendly tower
				}
			}
			else if (owner_->hasGroup(TwoTowers::melee))
			{
				if (owner_->hasGroup(TwoTowers::units))
				{

					//if melee friendly unit
				}
				else if (owner_->hasGroup(TwoTowers::towers))
				{

					//if friendly wall
				}
			}
			else if (owner_->hasGroup(TwoTowers::base_tower))
			{

				//if friendly base
			}
		}
	}
	void update() override
	{
		PlaySound("sounds\\music5.wav", 0, SND_FILENAME | SND_ASYNC);
		OutputDebugString((name + "\n").c_str());
		owner_->destroy();
	}
	void check() override
	{

	}
};

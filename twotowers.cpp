#include "TwoTowers.h"
#include "StateSeeking.h"
#include "StateAttacking.h"
#include "StateDying.h"

//#pragma comment(lib, "winmm.lib")

Manager main_menu_manager;
Manager mouse_manager;
Manager level_select_manager;
Manager victorymanager;
Manager game_manager;

//mainmenu entities
auto& mainmenubackground(main_menu_manager.addEntity());
auto& mainmenuplay(main_menu_manager.addEntity());
auto& mainmenuselect(main_menu_manager.addEntity());
auto& mainmenuexit(main_menu_manager.addEntity());

//level select entities
auto& level_select_background(level_select_manager.addEntity());
auto& level_select_1(level_select_manager.addEntity());
auto& level_select_2(level_select_manager.addEntity());
auto& level_select_back(level_select_manager.addEntity());

//victory screen entities
auto& victory_background(victorymanager.addEntity());
auto& victory_reset(victorymanager.addEntity());

//tower select entities
auto& cursor_tower(game_manager.addEntity());
auto& cursor_tower_2(game_manager.addEntity());
auto& towerselect1(game_manager.addEntity());
auto& towerselect2(game_manager.addEntity());
auto& unitselect1(game_manager.addEntity());
auto& unitselect2(game_manager.addEntity());
auto& levelplay(game_manager.addEntity());

//mouse entity
auto& cursormouse(mouse_manager.addEntity());

//level entities
auto& levelbackground(game_manager.addEntity());
auto& gameexit(game_manager.addEntity());

//rooms
bool mainmenu = true;
bool levelselect = false;
bool levelonescreen = false;
bool victory_Ascreen = false;

//tower select state check
bool firstcheck = false;
bool keydown1 = false;
bool keydown2 = false;
bool play = false;

//tower location saved
float og_x = 0;
float og_y = 0;

//Gold
float gold = 0;



//=============================================================================
// Constructor
//=============================================================================
TwoTowers::TwoTowers()
{
	dxFontSmall = new TextDX();     // DirectX fonts
	dxFontMedium = new TextDX();
	dxFontLarge = new TextDX();
	camera = new Camera(WIN_WIDTH, WIN_HEIGHT, 0, DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f));

}
//=============================================================================
// Destructor
//=============================================================================
TwoTowers::~TwoTowers()
{
	releaseAll();           // call onLostDevice() for every graphics item
}



//=============================================================================
// Initializes the game
// Throws GameError on error
//=============================================================================
void TwoTowers::initialize(HWND hwnd)
{
	Game::initialize(hwnd); // throws GameError
	//adding healthbar
	if (dxFontSmall->initialize(graphics, 15, true, false, "Arial") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));

	// 62 pixel high Arial
	if (dxFontMedium->initialize(graphics, 62, true, false, "Arial") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));

	// 124 pixel high Arial
	if (dxFontLarge->initialize(graphics, 124, true, false, "Arial") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));


	//main menu background
	mainmenubackground.addComponent<TransformComponent>(0, 0, WIN_WIDTH, WIN_HEIGHT, 1.5);
	mainmenubackground.addComponent<SpriteComponent>(graphics, MENUBACK_IMAGE, MENUBACK_WIDTH, MENUBACK_HEIGHT, MENUBACK_COLS);
	mainmenubackground.addGroup(main_menu);

	//Adding Menu Play Button
	mainmenuplay.addComponent<TransformComponent>((GAME_WIDTH / 1.75) - (MENU_PLAY_WIDTH / 4), (GAME_HEIGHT / 2.25)
		- (MENU_PLAY_HEIGHT / 4), MENU_PLAY_WIDTH, MENU_PLAY_HEIGHT, 0.25); //sets play button centered
	mainmenuplay.addComponent<SpriteComponent>(graphics, MENU_IMAGE, MENU_PLAY_WIDTH, MENU_PLAY_HEIGHT, MENU_PLAY_COLS);
	mainmenuplay.addComponent<InputComponent>(*input);
	mainmenuplay.getComponent<SpriteComponent>().SetCurrentFrame(MENU_PLAY_START_FRAME);
	mainmenuplay.getComponent<SpriteComponent>().SetCurrentFrame(MENU_EXIT_START_FRAME);
	mainmenuplay.addGroup(main_menu);

	//Adding Menu Level Select Button
	mainmenuselect.addComponent<TransformComponent>((GAME_WIDTH / 1.75) - (MENU_LEVEL_WIDTH / 4), (GAME_HEIGHT / 3.25)
		+ (MENU_LEVEL_HEIGHT / 2), MENU_LEVEL_WIDTH, MENU_LEVEL_HEIGHT, 0.25); //sets Exit button below play button
	mainmenuselect.addComponent<SpriteComponent>(graphics, MENU_IMAGE, MENU_LEVEL_WIDTH, MENU_LEVEL_HEIGHT, MENU_LEVEL_COLS);
	mainmenuselect.addComponent<InputComponent>(*input);
	mainmenuselect.getComponent<SpriteComponent>().SetCurrentFrame(MENU_LEVEL_START_FRAME);
	mainmenuselect.addGroup(main_menu);

	//Adding Menu Exit Button
	mainmenuexit.addComponent<TransformComponent>((GAME_WIDTH / 1.75) - (MENU_EXIT_WIDTH / 4), (GAME_HEIGHT / 2.5)
		+ (MENU_EXIT_HEIGHT / 2), MENU_EXIT_WIDTH, MENU_EXIT_HEIGHT, 0.25); //sets Exit button below play button
	mainmenuexit.addComponent<SpriteComponent>(graphics, MENU_IMAGE, MENU_EXIT_WIDTH, MENU_EXIT_HEIGHT, MENU_EXIT_COLS);
	mainmenuexit.addComponent<InputComponent>(*input);
	mainmenuexit.addGroup(main_menu);

	//Adding mouse
	cursormouse.addComponent<TransformComponent>(input->getMouseX(), input->getMouseY(), CURSOR_WIDTH, CURSOR_HEIGHT, 0.25);
	cursormouse.addComponent<SpriteComponent>(graphics, CURSOR_IMAGE, CURSOR_WIDTH, CURSOR_HEIGHT, CURSOR_COLS);
	
	//Adding first level
	levelbackground.addComponent<TransformComponent>(0, 0, GAME_WIDTH, GAME_HEIGHT, 1.0);
	levelbackground.addComponent<SpriteComponent>(graphics, LEVEL_ONE_BG_IMAGE, GAME_WIDTH, GAME_HEIGHT, 0);
	levelbackground.addGroup(level_one);
	
	//cursor tower
	cursor_tower.addComponent<TransformComponent>(GAME_WIDTH * 2, GAME_HEIGHT * 2, WALL_HEIGHT, WALL_WIDTH, 2.0);
	cursor_tower.addComponent<SpriteComponent>(graphics, WALL_IMAGE, WALL_HEIGHT, WALL_WIDTH, WALL_COLS);
	cursor_tower.getComponent<SpriteComponent>().InitializeAnimation(WALL_START_FRAME, WALL_END_FRAME, WALL_ANIMATION_DELAY);
	cursor_tower.addGroup(gui);

	//cursor tower 2
	cursor_tower_2.addComponent<TransformComponent>(GAME_WIDTH, GAME_HEIGHT, FRIENDLY_TOWER_HEIGHT, FRIENDLY_TOWER_WIDTH, 2.0);
	cursor_tower_2.addComponent<SpriteComponent>(graphics, FRIENDLY_TOWER_IMAGE, FRIENDLY_TOWER_HEIGHT, FRIENDLY_TOWER_WIDTH, FRIENDLY_TOWER_COLS);
	cursor_tower_2.getComponent<SpriteComponent>().InitializeAnimation(FRIENDLY_TOWER_START_FRAME, 0, FRIENDLY_TOWER_ANIMATION_DELAY);
	cursor_tower_2.addGroup(gui);
	//Adding game exit
	gameexit.addComponent<TransformComponent>((GAME_WIDTH / 2), (GAME_HEIGHT / 2), MENU_EXIT_WIDTH, MENU_EXIT_HEIGHT, 0.25);
	gameexit.addComponent<SpriteComponent>(graphics, MENU_IMAGE, MENU_EXIT_WIDTH, MENU_EXIT_HEIGHT, MENU_EXIT_COLS);
	gameexit.getComponent<SpriteComponent>().SetCurrentFrame(2);
	gameexit.addComponent<InputComponent>(*input);
	gameexit.addGroup(gui);

	//play gui
	levelplay.addComponent<TransformComponent>(camera->cameraX + WIN_WIDTH / 2, camera->cameraY + WIN_HEIGHT / 2, MENU_PLAY_HEIGHT, MENU_PLAY_WIDTH, 0.25);
	levelplay.addComponent<SpriteComponent>(graphics, MENU_IMAGE, MENU_PLAY_WIDTH, MENU_PLAY_HEIGHT, MENU_PLAY_COLS);
	levelplay.getComponent<SpriteComponent>().SetCurrentFrame(0);
	levelplay.addComponent<InputComponent>(*input);
	levelplay.addGroup(gui);
	//tower gui
	towerselect1.addComponent<TransformComponent>(camera->cameraX - WIN_WIDTH / 2 + (GUI_TOWER_WIDTH * 0), camera->cameraY - GUI_TOWER_HEIGHT, GUI_TOWER_WIDTH, GUI_TOWER_HEIGHT, 3.0);
	towerselect1.addComponent<SpriteComponent>(graphics, GUI_TOWER_IMAGE, GUI_TOWER_WIDTH, GUI_TOWER_HEIGHT, GUI_TOWER_COLS);
	towerselect1.getComponent<SpriteComponent>().SetCurrentFrame(GUI_TOWER_START_FRAME);
	towerselect1.addGroup(gui);

	towerselect2.addComponent<TransformComponent>(camera->cameraX - WIN_WIDTH / 2 + (GUI_TOWER_WIDTH * 1), camera->cameraY - GUI_TOWER_HEIGHT, GUI_TOWER_WIDTH, GUI_TOWER_HEIGHT, 3.0);
	towerselect2.addComponent<SpriteComponent>(graphics, GUI_TOWER_IMAGE, GUI_TOWER_WIDTH, GUI_TOWER_HEIGHT, GUI_TOWER_COLS);
	towerselect2.getComponent<SpriteComponent>().SetCurrentFrame(GUI_TOWER_START_FRAME_2);
	towerselect2.addGroup(gui);
	//unit gui
	unitselect1.addComponent<TransformComponent>(camera->cameraX - WIN_WIDTH / 2 + (GUI_UNIT_WIDTH * 0), camera->cameraY - GUI_UNIT_HEIGHT, GUI_UNIT_WIDTH, GUI_UNIT_HEIGHT, 3.0);
	unitselect1.addComponent<SpriteComponent>(graphics, GUI_UNIT_IMAGE, GUI_UNIT_WIDTH, GUI_UNIT_HEIGHT, GUI_UNIT_COLS);
	unitselect1.getComponent<SpriteComponent>().SetCurrentFrame(GUI_UNIT_START_FRAME);
	unitselect1.addGroup(gui);

	unitselect2.addComponent<TransformComponent>(camera->cameraX - WIN_WIDTH / 2 + (GUI_UNIT_WIDTH * 1), camera->cameraY - GUI_UNIT_HEIGHT, GUI_UNIT_WIDTH, GUI_UNIT_HEIGHT, 3.0);
	unitselect2.addComponent<SpriteComponent>(graphics, GUI_UNIT_IMAGE, GUI_UNIT_WIDTH, GUI_UNIT_HEIGHT, GUI_UNIT_COLS);
	unitselect2.getComponent<SpriteComponent>().SetCurrentFrame(GUI_UNIT_START_FRAME_2);
	unitselect2.addGroup(gui);

	//Level Select Background
	level_select_background.addComponent<TransformComponent>(0, 0, MENUBACK_WIDTH, MENUBACK_HEIGHT, 1.5);
	level_select_background.addComponent<SpriteComponent>(graphics, MENUBACK_IMAGE, MENUBACK_WIDTH, MENUBACK_HEIGHT, MENUBACK_COLS);
	level_select_background.addGroup(level_select);

	//Level 1 Select Button
	level_select_1.addComponent<TransformComponent>((GAME_WIDTH / 1.75) - (LEVEL1_WIDTH / 4), (GAME_HEIGHT / 2.10)
		- (LEVEL1_HEIGHT / 4), LEVEL1_WIDTH, LEVEL1_HEIGHT, 0.25);
	level_select_1.addComponent<SpriteComponent>(graphics, LEVEL_SELECT_IMAGE, LEVEL1_WIDTH, LEVEL1_HEIGHT, LEVEL1_COLS);
	level_select_1.getComponent<SpriteComponent>().SetCurrentFrame(LEVEL1_START_FRAME);
	level_select_1.addComponent<InputComponent>(*input);
	level_select_1.addGroup(level_select);

	//Level 2 Select Button
	level_select_2.addComponent<TransformComponent>((GAME_WIDTH / 1.75) - (LEVEL2_WIDTH / 4), (GAME_HEIGHT / 1.75)
		- (LEVEL2_HEIGHT / 4), LEVEL1_WIDTH, LEVEL1_HEIGHT, 0.25);
	level_select_2.addComponent<SpriteComponent>(graphics, LEVEL_SELECT_IMAGE, LEVEL2_WIDTH, LEVEL2_HEIGHT, LEVEL2_COLS);
	level_select_2.getComponent<SpriteComponent>().SetCurrentFrame(LEVEL2_START_FRAME);
	level_select_2.addComponent<InputComponent>(*input);
	level_select_2.addGroup(level_select);

	//Level Back Select Button
	level_select_back.addComponent<TransformComponent>((GAME_WIDTH / 1.75) - (LEVEL_BACK_WIDTH / 4), (GAME_HEIGHT / 1.5)
		- (LEVEL_BACK_HEIGHT / 4), LEVEL_BACK_WIDTH, LEVEL_BACK_HEIGHT, 0.25);
	level_select_back.addComponent<SpriteComponent>(graphics, LEVEL_SELECT_IMAGE, LEVEL_BACK_WIDTH, LEVEL_BACK_HEIGHT, LEVEL_BACK_COLS);
	level_select_back.getComponent<SpriteComponent>().SetCurrentFrame(LEVEL_BACK_START_FRAME);
	level_select_back.addComponent<InputComponent>(*input);
	level_select_back.addGroup(level_select);

	//Victory Background
	victory_background.addComponent<TransformComponent>(0, 0, MENUBACK_WIDTH, MENUBACK_HEIGHT, 1.5);
	victory_background.addComponent<SpriteComponent>(graphics, MENUBACK_IMAGE, MENUBACK_WIDTH, MENUBACK_HEIGHT, MENUBACK_COLS);
	victory_background.addGroup(victory);

	//Victory Reset button
	victory_reset.addComponent<TransformComponent>((GAME_WIDTH / 1.75) - (LEVEL_BACK_WIDTH / 4), (GAME_HEIGHT / 1.5)
		- (LEVEL_BACK_HEIGHT / 4), LEVEL_BACK_WIDTH, LEVEL_BACK_HEIGHT, 0.25);
	victory_reset.addComponent<SpriteComponent>(graphics, LEVEL_SELECT_IMAGE, LEVEL_BACK_WIDTH, LEVEL_BACK_HEIGHT, LEVEL_BACK_COLS);
	victory_reset.getComponent<SpriteComponent>().SetCurrentFrame(LEVEL_BACK_START_FRAME);
	victory_reset.addComponent<InputComponent>(*input);
	victory_reset.addGroup(victory);
	tower_manager = new TowerManager(&game_manager, graphics, input, projectile_manager);
	projectile_manager = new ProjectileManager(&game_manager, graphics);
	nodegraph = new NodeGraph("output.txt", tower_manager);
	nodegraph->drawGraph(graphics, &game_manager);
	unit_manager = new UnitManager(&game_manager, graphics, input, projectile_manager, nodegraph);
	nodegraph->drawWalls();
	nodegraph->pathFindAStar(nodegraph->node_array_[11], nodegraph->node_array_[10]);
	//diables cursor
	ShowCursor(false);
	return;
}

//=============================================================================
// Update all game items
//=============================================================================
void TwoTowers::update()
{
	//tower.getComponent<TransformComponent>().updatePosition(input->getMouseX(), input->getMouseY());
	cursormouse.getComponent<TransformComponent>().updatePosition(input->getMouseX(), input->getMouseY());
	mouse_manager.refresh();
	mouse_manager.update(frameTime);
	//Towers test
	if (input->wasKeyPressed('W'))
	{
		victory_Ascreen = true;
	}

	//updating mainmenu manager
	if (mainmenu == true)
	{
		main_menu_manager.refresh();
		main_menu_manager.update(frameTime);
		camera->Follow(&mainmenubackground);
		camera->Update();

		//check if play button is pressed
		if (mainmenuplay.getComponent<InputComponent>().isClickedOn())
		{
			//Animation for Play button
			mainmenuplay.getComponent<SpriteComponent>().SetCurrentFrame(MENU_PLAY_END_FRAME);
			if (mainmenuplay.getComponent<InputComponent>().getInputFlag())
			{

				PlaySound("sounds\\music1.wav", 0, SND_FILENAME | SND_ASYNC | SND_LOOP);
				/*PlaySound("sounds\\music2.wav", 0, SND_FILENAME | SND_ASYNC);*/
				levelonescreen = true;
				levelselect = false;
				mainmenu = false;
				firstcheck = true;
			}
		}
		else
		{
			mainmenuplay.getComponent<SpriteComponent>().SetCurrentFrame(MENU_PLAY_START_FRAME);
		}

		//check if level select button is pressed
		if (mainmenuselect.getComponent<InputComponent>().isClickedOn())
		{
			//Animation for Level Select button
			mainmenuselect.getComponent<SpriteComponent>().SetCurrentFrame(MENU_LEVEL_END_FRAME);
			if (mainmenuselect.getComponent<InputComponent>().getInputFlag())
			{
				levelonescreen = false;
				levelselect = true;
				mainmenu = false;
			}
		}
		else
		{
			mainmenuselect.getComponent<SpriteComponent>().SetCurrentFrame(MENU_LEVEL_START_FRAME);
		}

		//check if exit button is pressed
		if (mainmenuexit.getComponent<InputComponent>().isClickedOn())
		{
			//Animation for Exit button
			mainmenuexit.getComponent<SpriteComponent>().SetCurrentFrame(MENU_EXIT_END_FRAME);
			if (mainmenuexit.getComponent<InputComponent>().getInputFlag())
			{
				PostQuitMessage(0);
			}
		}
		else
		{
			mainmenuexit.getComponent<SpriteComponent>().SetCurrentFrame(MENU_EXIT_START_FRAME);
		}
	}

	if (levelselect)
	{
		level_select_manager.refresh();
		level_select_manager.update(frameTime);
		camera->Follow(&level_select_background);
		camera->Update();

		//check if level 1 button is pressed
		if (level_select_1.getComponent<InputComponent>().isClickedOn())
		{
			//Animation for Play button
			level_select_1.getComponent<SpriteComponent>().SetCurrentFrame(LEVEL1_END_FRAME);
			if (level_select_1.getComponent<InputComponent>().getInputFlag())
			{
				PlaySound("sounds\\music1.wav", 0, SND_FILENAME | SND_ASYNC);
				
				/*PlaySound("sounds\\music2.wav", 0, SND_FILENAME | SND_ASYNC);*/
				levelonescreen = true;
				levelselect = false;
				mainmenu = false;
			}
		}
		else
		{
			level_select_1.getComponent<SpriteComponent>().SetCurrentFrame(LEVEL1_START_FRAME);
		}

		//check if level select button is pressed
		if (level_select_2.getComponent<InputComponent>().isClickedOn())
		{
			//Animation for Level Select button
			level_select_2.getComponent<SpriteComponent>().SetCurrentFrame(LEVEL2_END_FRAME);
			if (level_select_2.getComponent<InputComponent>().getInputFlag())
			{
				//insert level 2 code here
			}
		}
		else
		{
			level_select_2.getComponent<SpriteComponent>().SetCurrentFrame(LEVEL2_START_FRAME);
		}

		//check if exit button is pressed
		if (level_select_back.getComponent<InputComponent>().isClickedOn())
		{
			//Animation for Exit button
			level_select_back.getComponent<SpriteComponent>().SetCurrentFrame(LEVEL_BACK_END_FRAME);
			if (level_select_back.getComponent<InputComponent>().getInputFlag())
			{
				levelonescreen = false;
				levelselect = false;
				mainmenu = true;
			}
		}
		else
		{
			level_select_back.getComponent<SpriteComponent>().SetCurrentFrame(LEVEL_BACK_START_FRAME);
		}
	}


	if (levelonescreen)
	{
		camera->setCameraScreen(WIN_WIDTH, WIN_HEIGHT);
		camera->Follow(&cursormouse);

		game_manager.refresh();
		game_manager.update(frameTime);

		VECTOR2 firstnode;
		VECTOR2 lastnode;
		//firstnode and last node
		for (auto& n : game_manager.getGroup(base_tower))
		{
			if (n->hasGroup(friendly))
			{
				firstnode = n->getComponent<TransformComponent>().position;
			}
			else if (n->hasGroup(enemy))
			{
				lastnode = n->getComponent<TransformComponent>().position;
			}
		}
		
		//freezes camera if player is holding right click
		if (input->getMouseRButton())
		{

		}
		else
		{
			camera->Update();
		}

		//add 10 gold every second
		gold += 5 * frameTime;

		//play button gui
		levelplay.getComponent<TransformComponent>().updatePosition(camera->cameraX + WIN_WIDTH / 2 - (MENU_PLAY_WIDTH * levelplay.getComponent<TransformComponent>().scale),
			camera->cameraY + WIN_HEIGHT / 2 - (MENU_PLAY_HEIGHT * levelplay.getComponent<TransformComponent>().scale));

		//checks play state
		if (levelplay.getComponent<InputComponent>().isClickedOn())
		{
			if (levelplay.getComponent<InputComponent>().getInputFlag())
			{
				play = true;
			}
		}

		//checks if player presses 1
		if (input->wasKeyPressed('1'))
		{
			keydown1 = true;

			//checks if 2 was pressed, if yes, return the tower back to original position
			if (keydown2 == true)
			{
				cursor_tower_2.getComponent<TransformComponent>().updatePosition(GAME_WIDTH, GAME_HEIGHT);
				keydown2 = false;
			}
		}

		//checks if player presses 2
		if (input->wasKeyPressed('2'))
		{
			keydown2 = true;

			//checks if 1 was pressed, if yes, return the tower back to original position
			if (keydown1 == true)
			{
				cursor_tower.getComponent<TransformComponent>().updatePosition(GAME_WIDTH, GAME_HEIGHT);
				keydown1 = false;
			}
		}
		//if play state is true
		if (play)
		{
			//tower select 1 gui
			towerselect1.getComponent<TransformComponent>().updatePosition(GAME_WIDTH, GAME_HEIGHT);

			//tower select 2 gui
			towerselect2.getComponent<TransformComponent>().updatePosition(GAME_WIDTH, GAME_HEIGHT);

			//unit select 1 gui
			unitselect1.getComponent<TransformComponent>().updatePosition(camera->cameraX - WIN_WIDTH / 2 +
				(GUI_UNIT_WIDTH * unitselect1.getComponent<TransformComponent>().scale * 0),
				camera->cameraY + WIN_HEIGHT / 2 - (GUI_UNIT_HEIGHT * unitselect1.getComponent<TransformComponent>().scale));

			//unit select 2 gui
			unitselect2.getComponent<TransformComponent>().updatePosition(camera->cameraX - WIN_WIDTH / 2 +
				(GUI_UNIT_WIDTH * unitselect2.getComponent<TransformComponent>().scale * 1),
				camera->cameraY + WIN_HEIGHT / 2 - (GUI_UNIT_HEIGHT * unitselect2.getComponent<TransformComponent>().scale));

			//checks if '1' was pressed and gold >= 50
			if (keydown1 == true && gold >= UNIT1_PRICE)
			{
				//changes gui tower select
				unitselect1.getComponent<SpriteComponent>().SetCurrentFrame(GUI_UNIT_END_FRAME);

				if (input->getMouseLButton())
				{
					unit_manager->createFriendlyKnight(firstnode);
					unit_manager->createEnemyKnight(lastnode);
					gold -= UNIT1_PRICE;
					keydown1 = false;
				}
			}
			else
			{
				//changes gui tower select
				unitselect1.getComponent<SpriteComponent>().SetCurrentFrame(GUI_UNIT_START_FRAME);
				keydown1 = false;
			}

			//checks if 2 was pressed and gold >= 100
			if (keydown2 == true && gold >= UNIT2_PRICE)
			{
				//changes gui tower select
				unitselect2.getComponent<SpriteComponent>().SetCurrentFrame(GUI_UNIT_END_FRAME_2);

				//checks if left mouse is pressed
				if (input->getMouseLButton())
				{
					unit_manager->createFriendlyMarine(firstnode);
					unit_manager->createEnemyMarine(lastnode);
					gold -= UNIT2_PRICE;
					keydown2 = false;
				}
			}
			else
			{
				//changes gui tower select
				unitselect2.getComponent<SpriteComponent>().SetCurrentFrame(GUI_UNIT_START_FRAME_2);
				keydown2 = false;
			}
		}
		else
		{
			//unit select 1 gui
			unitselect1.getComponent<TransformComponent>().updatePosition(GAME_WIDTH, GAME_HEIGHT);

			//unit select 2 gui
			unitselect2.getComponent<TransformComponent>().updatePosition(GAME_WIDTH, GAME_HEIGHT);

			//tower select 1 gui
			towerselect1.getComponent<TransformComponent>().updatePosition(camera->cameraX - WIN_WIDTH / 2 +
				(GUI_TOWER_WIDTH * towerselect1.getComponent<TransformComponent>().scale * 0),
				camera->cameraY + WIN_HEIGHT / 2 - (GUI_TOWER_HEIGHT * towerselect1.getComponent<TransformComponent>().scale));

			//tower select 2 gui
			towerselect2.getComponent<TransformComponent>().updatePosition(camera->cameraX - WIN_WIDTH / 2 +
				(GUI_TOWER_WIDTH * towerselect2.getComponent<TransformComponent>().scale * 1),
				camera->cameraY + WIN_HEIGHT / 2 - (GUI_TOWER_HEIGHT * towerselect2.getComponent<TransformComponent>().scale));

			//checks if '1' was pressed and gold >= 50
			if (keydown1 == true && gold >= TOWER1_PRICE)
			{
				//makes the tower follow cursor
				cursor_tower.getComponent<TransformComponent>().updatePosition(cursormouse.getComponent<TransformComponent>().position.x,
					cursormouse.getComponent<TransformComponent>().position.y);

				//changes gui tower select
				towerselect1.getComponent<SpriteComponent>().SetCurrentFrame(GUI_TOWER_END_FRAME);

				//if left mouse button is clicked, create a new tower to place at that location
				//and resets the keypress
				//if right mouse button is clicked, the tower will return to original position and clear key press
				if (input->getMouseLButton())
				{

					Node* nearest_node = nodegraph->getNearestNode(cursormouse.getComponent<TransformComponent>().position);
					nearest_node->changeActive(false);
					tower_manager->createFriendlyWall(nearest_node->position_);
					nodegraph->drawWalls();
					cursor_tower.getComponent<TransformComponent>().updatePosition(GAME_WIDTH, GAME_HEIGHT);
					cursor_tower_2.getComponent<TransformComponent>().updatePosition(GAME_WIDTH, GAME_HEIGHT);
					gold -= TOWER1_PRICE;
					keydown1 = false;
				}
				else if (input->getMouseRButton())
				{
					cursor_tower.getComponent<TransformComponent>().updatePosition(GAME_WIDTH, GAME_HEIGHT);
					cursor_tower_2.getComponent<TransformComponent>().updatePosition(GAME_WIDTH, GAME_HEIGHT);
					keydown1 = false;
				}
			}
			else
			{
				//changes gui tower select
				towerselect1.getComponent<SpriteComponent>().SetCurrentFrame(GUI_TOWER_START_FRAME);
			}

			//checks if 2 was pressed and gold >= 100
			if (keydown2 == true && gold >= TOWER2_PRICE)
			{
				//makes the tower follow cursor
				cursor_tower_2.getComponent<TransformComponent>().updatePosition(cursormouse.getComponent<TransformComponent>().position.x,
					cursormouse.getComponent<TransformComponent>().position.y);

				//changes gui tower select
				towerselect2.getComponent<SpriteComponent>().SetCurrentFrame(GUI_TOWER_END_FRAME_2);

				//if left mouse button is clicked, create a new tower to place at that location
				//and resets the keypress
				//if right mouse button is clicked, the tower will return to original position and clear key press
				if (input->getMouseLButton())
				{
					Node* nearest_node = nodegraph->getNearestNode(cursormouse.getComponent<TransformComponent>().position);
					nearest_node->changeActive(false);
					tower_manager->createFriendlyTurret(nearest_node->position_);
					nodegraph->drawWalls();


					cursor_tower_2.getComponent<TransformComponent>().updatePosition(GAME_WIDTH, GAME_HEIGHT);
					gold -= TOWER2_PRICE;
					keydown2 = false;
				}
				else if (input->getMouseRButton())
				{
					cursor_tower_2.getComponent<TransformComponent>().updatePosition(GAME_WIDTH, GAME_HEIGHT);
					keydown2 = false;
				}
			}
			else
			{
				//changes gui tower select
				towerselect2.getComponent<SpriteComponent>().SetCurrentFrame(GUI_TOWER_START_FRAME_2);
			}
		}

		//checking for rooms
		if (gameexit.getComponent<InputComponent>().isClickedOn())
		{
			if (gameexit.getComponent<InputComponent>().getInputFlag())
			{
				PlaySound(NULL, 0, 0);
				mainmenu = true;
				levelselect = false;
				levelonescreen = false;
			}
		}
	}
	if (victory_Ascreen)
	{
		mainmenu = false;
		levelselect = false;
		levelonescreen = false;
		victorymanager.refresh();
		victorymanager.update(frameTime);
		camera->Follow(&victory_background);
		camera->Update();
		if (victory_reset.getComponent<InputComponent>().isClickedOn())
		{
			//Animation for Reset button
			victory_reset.getComponent<SpriteComponent>().SetCurrentFrame(LEVEL_BACK_END_FRAME);
			if (victory_reset.getComponent<InputComponent>().getInputFlag())
			{
				PostQuitMessage(0);
			}
		}
		else
		{
			victory_reset.getComponent<SpriteComponent>().SetCurrentFrame(LEVEL_BACK_START_FRAME);
		}
	}

}


//=============================================================================
// Artificial Intelligence
//=============================================================================
void TwoTowers::ai()
{

}

//=============================================================================
// Handle collisions
//=============================================================================
void TwoTowers::collisions()
{

}

//=============================================================================
// Render game items
//=============================================================================
void TwoTowers::render()
{
	graphics->spriteBegin();
	camera->SetTransform(graphics);
	int count = 0;
	//drawing every sprite in menus group(mainmenu)
	if (mainmenu)
	{
		for (auto& p : main_menu_manager.getGroup(main_menu))
		{
			p->draw();
		}
	}
	//drawing every sprite in level_select group(level select)
	else if (levelselect)
	{
		for (auto& p : level_select_manager.getGroup(level_select))
		{
			p->draw();
		}
	}
	//drawing every sprite in level one
	else if (levelonescreen)
	{
		for (auto& p : game_manager.getGroup(level_one))
		{
			p->draw();
		}
		for (auto& p : game_manager.getGroup(tiles))
		{
			p->draw();
		}
		for (auto& p : game_manager.getGroup(walls))
		{
			p->draw();
		}
		for (auto& p : game_manager.getGroup(towers))
		{
			p->draw();
		}
		for (auto& p : game_manager.getGroup(units))
		{
			p->draw();
		}
		for (auto& p : game_manager.getGroup(gui))
		{
			p->draw();
		}
		for (auto& p : game_manager.getGroup(projectiles))
		{
			p->draw();
		}
		//renders gold when in game
		int igold = int(gold + 0.5f);
		dxFontMedium->print(std::to_string(igold), camera->cameraX - WIN_WIDTH / 2, camera->cameraY - WIN_HEIGHT / 2);
	}
	else if (victory_Ascreen)
	{
		for (auto& p : victorymanager.getGroup(victory))
		{
			p->draw();
		}
	}

	//drawing the tower for placement if key 1/2 is pressed and gold more than fixed amount
	if (keydown1 && gold >= TOWER1_PRICE)
	{
		cursor_tower.draw();
	}
	else if (keydown2 && gold >= TOWER2_PRICE)
	{
		cursor_tower_2.draw();
	}
	//Rendering of cursor
	cursormouse.draw();

	graphics->spriteEnd();                  // end drawing sprites

}

//=============================================================================
// The graphics device was lost.
// Release all reserved video memory so graphics device may be reset.
//=============================================================================
void TwoTowers::releaseAll()
{
	if (camera) { delete camera; camera = nullptr; }

	Game::releaseAll();
	return;
}

//=============================================================================
// The grahics device has been reset.
// Recreate all surfaces.
//=============================================================================
void TwoTowers::resetAll()
{
	Game::resetAll();
	return;
}




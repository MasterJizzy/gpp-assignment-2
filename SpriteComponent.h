#pragma once
#include "image.h"
#include "Components.h"
#include "constants.h"
#include "TextureManager.h"
//#include "TransformComponent.h"
#include "game.h"

class SpriteComponent: public Component
{
private:
	TransformComponent *transform_;
	TextureManager texture_;
	Graphics *graphics;
	Image image_;
	std::string path_location_;
	int width_;
	int height_;
	int columns_;
	bool initialized_ = false;

	bool animated = false;
	int frames = 0;
	int speed = 100;

public:

	SpriteComponent() = default;
	SpriteComponent(Graphics* g, const std::string& path, const int& width, const int& height, const int& columns)
		:
		path_location_(path),
		width_(width),
		height_(height),
		columns_(columns)
	{
		graphics = g;
	}

	/*SpriteComponent(const std::string& path, int nFrames, int mSpeed)
		:
		path_location_(path),
		frames(nFrames),
		speed(mSpeed)
	{
		
		animated = true;
		
		
	}*/

	~SpriteComponent()
	{

	}

	void init() override
	{
		if (graphics != nullptr) {
			texture_.initialize(graphics, path_location_.c_str());
			image_.initialize(graphics, width_, height_, columns_, &texture_);
			transform_ = &entity->getComponent<TransformComponent>();
			initialized_ = true;
		}
	}
	void update(const float& frametime) override
	{

		if (animated)
		{
			//srcRect.x = srcRect.w* static_cast<int>((SDL_GetTicks() / speed % frames));
		}
		if (initialized_) {
			image_.setX(transform_->position.x);
			image_.setY(transform_->position.y);
			image_.setScale(transform_->scale);
			image_.update(frametime);
		}
	}

	void draw() override
	{
		if (initialized_) {
			image_.draw();
		}
	}

	void InitializeAnimation(const int& startframe, const int& endframe, const float& animationdelay)
	{
		image_.setFrames(startframe, endframe);
		image_.setFrameDelay(animationdelay);
		image_.setCurrentFrame(startframe);
	}

	// Get functions
	Image& GetImage()
	{
		return image_;
	}

	int GetWidth()
	{
		return width_;
	}

	int GetHeight()
	{
		return height_;
	}

	// Set functions
	void SetX(int x)
	{
		image_.setX(x);
	}

	void SetY(int y)
	{
		image_.setY(y);
	}

	void SetCurrentFrame(int c)
	{
		image_.setCurrentFrame(c);
		image_.setRect();
	}
	float getCenterX()
	{
		return entity->getComponent<TransformComponent>().position.x + width_ / 2 * entity->getComponent<TransformComponent>().scale;
	}

	// Return center Y.
	float getCenterY()
	{
		return entity->getComponent<TransformComponent>().position.y + height_ / 2 * entity->getComponent<TransformComponent>().scale;
	}
};


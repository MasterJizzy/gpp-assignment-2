#include "ProjectileManager.h"
#include "Components.h"
#include "twotowers.h"



ProjectileManager::ProjectileManager(Manager* man, Graphics* gra) : manager(man), graphics(gra)
{}

ProjectileManager::~ProjectileManager()
{

}

void ProjectileManager::CreateProjectile(VECTOR2 pos, VECTOR2 vel, Entity* owner)
{

	auto& projectile(manager->addEntity( ));
	projectile.addComponent<TransformComponent>(pos.x, pos.y, 32, 32, 1);
	projectile.addComponent<SpriteComponent>(graphics, FRIENDLY_TOWER_PROJECTILE_IMAGE, FRIENDLY_TOWER_HEIGHT, FRIENDLY_TOWER_WIDTH, 0);
	projectile.addComponent<CollisionComponent>();
	projectile.addComponent<PhysicsComponent>(); 
	projectile.getComponent<PhysicsComponent>().SetVelocity(vel);

	projectile.addGroup(TwoTowers::projectiles);
}
bool ProjectileManager::CheckProjectile(Entity* target)
{
	auto& entityarr(manager->getGroup(TwoTowers::projectiles));
	for (auto& e : entityarr)
	{
		if (e->getComponent<CollisionComponent>().checkCollision(target))
		{
			e->destroy();
			return true;
		}
	}
}

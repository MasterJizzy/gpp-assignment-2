#include "UnitManager.h"
#include "Components.h"
#include "twotowers.h"
#include "StateAttacking.h"
#include "StateSeeking.h"
#include "StateDying.h"
#include "ProjectileManager.h"



UnitManager::UnitManager(Manager* man, Graphics* gra, Input* in, ProjectileManager* p, NodeGraph* n)
	:
	pmanager(p),
	manager(man),
	graphics(gra),
	input(in),
	nodegraph(n)
{

}

UnitManager::~UnitManager()
{

}
void UnitManager::createFriendlyMarine(VECTOR2 position)
{
	auto& marine(manager->addEntity());
	marine.addComponent<TransformComponent>(position.x, position.y, ROBOT_HEIGHT, ROBOT_WIDTH, 3.0);
	marine.addComponent<SpriteComponent>(graphics, ROBOT_IMAGE, ROBOT_HEIGHT, ROBOT_WIDTH, ROBOT_COLS);
	marine.getComponent<SpriteComponent>().InitializeAnimation(ROBOT_START_FRAME, ROBOT_END_FRAME, ROBOT_ANIMATION_DELAY);
	marine.addComponent<HealthComponent>(100, 100, graphics);
	marine.addComponent<PhysicsComponent>();
	marine.addComponent<InputComponent>(*input);
	for (auto& e : manager->getGroup(TwoTowers::base_tower))
	{
		if (e->hasGroup(TwoTowers::enemy))
		{
			marine.addComponent<StateComponent>(new StateSeeking(&marine, "seeking", manager, nodegraph, e->getComponent<TransformComponent>().position));
			break;

		}
	}
	marine.getComponent<StateComponent>().addState(new StateAttacking(&marine, "attacking", manager, pmanager));
	marine.getComponent<StateComponent>().addState(new StateAttacking(&marine, "attacking", manager, pmanager));
	marine.addGroup(TwoTowers::friendly);
	marine.addGroup(TwoTowers::units);
	marine.addGroup(TwoTowers::ranged);
}
void UnitManager::createFriendlyKnight(VECTOR2 position)
{
	auto& knight(manager->addEntity());
	knight.addComponent<TransformComponent>(position.x,position.y, BAT_HEIGHT, BAT_WIDTH, 3.0);
	knight.addComponent<SpriteComponent>(graphics, BAT_IMAGE, BAT_HEIGHT, BAT_WIDTH, BAT_COLS);
	knight.getComponent<SpriteComponent>().InitializeAnimation(ROBOT_START_FRAME, ROBOT_END_FRAME, ROBOT_ANIMATION_DELAY);
	knight.addComponent<HealthComponent>(100, 100, graphics);
	knight.addComponent<InputComponent>(*input);
	knight.addComponent<PhysicsComponent>();
	for (auto& e : manager->getGroup(TwoTowers::base_tower))
	{
		if (e->hasGroup(TwoTowers::enemy))
		{
			knight.addComponent<StateComponent>(new StateSeeking(&knight, "seeking", manager, nodegraph, e->getComponent<TransformComponent>().position));
			break;

		}
	}
	knight.getComponent<StateComponent>().addState(new StateAttacking(&knight, "attacking", manager, pmanager));
	knight.addGroup(TwoTowers::friendly);
	knight.addGroup(TwoTowers::units);
	knight.addGroup(TwoTowers::ranged);
}
void UnitManager::createEnemyMarine(VECTOR2 position)
{
	auto& marine(manager->addEntity());
	marine.addComponent<TransformComponent>(position.x, position.y, ROBOT_HEIGHT, ROBOT_WIDTH, 3.0);
	marine.addComponent<SpriteComponent>(graphics, ROBOT_IMAGE, ROBOT_HEIGHT, ROBOT_WIDTH, ROBOT_COLS);
	marine.getComponent<SpriteComponent>().InitializeAnimation(ROBOT_START_FRAME, ROBOT_END_FRAME, ROBOT_ANIMATION_DELAY);
	marine.addComponent<HealthComponent>(100, 100, graphics);
	marine.addComponent<InputComponent>(*input);

	for (auto& e : manager->getGroup(TwoTowers::base_tower))
	{
		if (e->hasGroup(TwoTowers::friendly))
		{
			marine.addComponent<StateComponent>(new StateSeeking(&marine, "seeking", manager, nodegraph, e->getComponent<TransformComponent>().position));
			break;

		}
	}
	marine.getComponent<StateComponent>().addState(new StateAttacking(&marine, "attacking", manager, pmanager));
	marine.addComponent<PhysicsComponent>();
	marine.addGroup(TwoTowers::enemy);
	marine.addGroup(TwoTowers::units);
	marine.addGroup(TwoTowers::ranged);
}
void UnitManager::createEnemyKnight(VECTOR2 position)
{
	auto& knight(manager->addEntity());
	knight.addComponent<TransformComponent>(position.x, position.y, BAT_HEIGHT, BAT_WIDTH, 3.0);
	knight.addComponent<SpriteComponent>(graphics, BAT_IMAGE, BAT_HEIGHT, BAT_WIDTH, BAT_COLS);
	knight.getComponent<SpriteComponent>().InitializeAnimation(ROBOT_START_FRAME, ROBOT_END_FRAME, ROBOT_ANIMATION_DELAY);
	knight.addComponent<HealthComponent>(100, 100, graphics);
	knight.addComponent<InputComponent>(*input);
	knight.addComponent<PhysicsComponent>();

	for (auto& e : manager->getGroup(TwoTowers::base_tower))
	{
		if (e->hasGroup(TwoTowers::friendly))
		{
			knight.addComponent<StateComponent>(new StateSeeking(&knight, "seeking", manager, nodegraph, e->getComponent<TransformComponent>().position));
			break;

		}
	}
	knight.getComponent<StateComponent>().addState(new StateAttacking(&knight, "attacking", manager, pmanager));
	knight.addGroup(TwoTowers::enemy);
	knight.addGroup(TwoTowers::units);
	knight.addGroup(TwoTowers::melee);
}

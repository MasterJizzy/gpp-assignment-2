#include "stack.h"
Stack::Stack()
{
	topNode = NULL;
}


Stack::~Stack()
{	
}
//check if the stack is empty
bool Stack::isEmpty()
{
	if (topNode == NULL)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//push item on top of the stack
bool Stack::push(string& item)
{
	Node* newNode = new Node;
	newNode->item = item;
	newNode->next = NULL;
	if (topNode == NULL)
	{
		topNode = newNode;
		return true;
	}
	else
	{
		newNode->next = topNode;
		topNode = newNode;
		return true;
	}
}

//pop item from top of stack
bool Stack::pop()
{
	if (topNode != NULL)
	{
		Node* curr = new Node;
		curr = topNode;
		topNode = curr->next;
		curr->next = NULL;
		delete curr;
		return true;
	}
	else
	{
		return false;
	}
}

//retrieve and pop item from top of stack
bool Stack::pop(string& item)
{
	if (topNode != NULL)
	{
		Node* curr = new Node;
		Node* prev = new Node;
		curr = topNode;
		while (curr->item != item)
		{
			prev = curr;
			curr = curr->next;
			if (curr->item == item)
			{
				prev->next = prev->next->next;
				curr->next = NULL;
				string value = curr->item;
				delete curr;
				return true;
				break;
			}
		}
	}

}

//retrieve item from top of stack
string Stack::getTop()
{
	Node* curr = new Node;
	curr = topNode;
	string value = topNode->item;
	return value;
}




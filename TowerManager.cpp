#include "TowerManager.h"
#include "Components.h"
#include "twotowers.h"
#include "StateAttacking.h"
#include "StateSeeking.h"
#include "StateDying.h"
#include  "ProjectileManager.h"

TowerManager::TowerManager(Manager* man, Graphics* gra, Input* in, ProjectileManager* p)
	:
	pmanager(p),
	manager(man),
	graphics(gra),
	input(in)
{

}

TowerManager::~TowerManager()
{

}
void TowerManager::createFriendlyTurret(VECTOR2 position)
{
	auto& turret(manager->addEntity());
	turret.addGroup(TwoTowers::friendly);
	turret.addGroup(TwoTowers::towers);
	turret.addGroup(TwoTowers::ranged);
	turret.addComponent<TransformComponent>(position.x, position.y, FRIENDLY_TOWER_WIDTH, FRIENDLY_TOWER_HEIGHT, 2.0);
	turret.addComponent<SpriteComponent>(graphics, FRIENDLY_TOWER_IMAGE, FRIENDLY_TOWER_WIDTH, FRIENDLY_TOWER_HEIGHT, FRIENDLY_TOWER_COLS);
	turret.getComponent<SpriteComponent>().SetCurrentFrame(FRIENDLY_TOWER_START_FRAME);
	turret.addComponent<InputComponent>(*input);
	turret.addComponent<StateComponent>(new StateSeeking(&turret, "seeking", manager));
	turret.getComponent<StateComponent>().addState(new StateAttacking(&turret, "attacking", manager, pmanager));

}
void TowerManager::createFriendlyWall(VECTOR2 position)
{
	auto& wall(manager->addEntity());
	wall.addGroup(TwoTowers::friendly);
	wall.addGroup(TwoTowers::towers);
	wall.addGroup(TwoTowers::melee);
	wall.addComponent<TransformComponent>(position.x, position.y, WALL_HEIGHT, WALL_WIDTH, 2.0);
	wall.addComponent<SpriteComponent>(graphics, WALL_IMAGE, WALL_WIDTH, WALL_HEIGHT, WALL_COLS);
	wall.getComponent<SpriteComponent>().InitializeAnimation(WALL_START_FRAME, 0, WALL_ANIMATION_DELAY);
	wall.addComponent<InputComponent>(*input);

}
void TowerManager::createFriendlyBase(VECTOR2 position)
{
	auto& base(manager->addEntity());
	base.addComponent<HealthComponent>(100, 100, graphics);
	base.addGroup(TwoTowers::friendly);
	base.addGroup(TwoTowers::towers);
	base.addGroup(TwoTowers::base_tower);
	base.addComponent<TransformComponent>(position.x, position.y, WALL_HEIGHT, WALL_WIDTH, 2.0);
	base.addComponent<SpriteComponent>(graphics, FRIENDLY_BASE_IMAGE, WALL_WIDTH, WALL_HEIGHT, WALL_COLS);
	base.getComponent<SpriteComponent>().InitializeAnimation(WALL_START_FRAME, 0, WALL_ANIMATION_DELAY);
	base.addComponent<InputComponent>(*input);

}
void TowerManager::createEnemyTurret(VECTOR2 position)
{
	auto& turret(manager->addEntity());
	turret.addGroup(TwoTowers::enemy);
	turret.addGroup(TwoTowers::towers);
	turret.addGroup(TwoTowers::ranged);
	turret.addComponent<TransformComponent>(position.x, position.y, ENEMY_TOWER_HEIGHT, ENEMY_TOWER_WIDTH, 2.0);
	turret.addComponent<SpriteComponent>(graphics, ENEMY_TOWER_IMAGE, ENEMY_TOWER_WIDTH, ENEMY_TOWER_HEIGHT, ENEMY_TOWER_COLS);
	turret.getComponent<SpriteComponent>().InitializeAnimation(ENEMY_TOWER_START_FRAME, ENEMY_TOWER_END_FRAME, ENEMY_TOWER_ANIMATION_DELAY);
	turret.addComponent<InputComponent>(*input);
	turret.addComponent<StateComponent>(new StateSeeking(&turret, "seeking", manager));
	turret.getComponent<StateComponent>().addState(new StateAttacking(&turret, "attacking", manager, pmanager));
}
void TowerManager::createEnemyWall(VECTOR2 position)
{
	auto& wall(manager->addEntity());
	wall.addGroup(TwoTowers::enemy);
	wall.addGroup(TwoTowers::towers);
	wall.addGroup(TwoTowers::melee);
	wall.addComponent<TransformComponent>(position.x, position.y, WALL_HEIGHT, WALL_WIDTH, 2.0);
	wall.addComponent<SpriteComponent>(graphics, WALL_IMAGE, WALL_WIDTH, WALL_HEIGHT, WALL_COLS);
	wall.getComponent<SpriteComponent>().InitializeAnimation(WALL_START_FRAME, 0, WALL_ANIMATION_DELAY);
	wall.addComponent<InputComponent>(*input);

}
void TowerManager::createEnemyBase(VECTOR2 position)
{
	auto& base(manager->addEntity());
	base.addGroup(TwoTowers::enemy);
	base.addGroup(TwoTowers::towers);
	base.addGroup(TwoTowers::base_tower);
	base.addComponent<TransformComponent>(position.x, position.y, WALL_HEIGHT, WALL_WIDTH, 2.0);
	base.addComponent<SpriteComponent>(graphics, ENEMY_BASE_IMAGE, WALL_WIDTH,WALL_HEIGHT, WALL_COLS);
	base.getComponent<SpriteComponent>().InitializeAnimation(WALL_START_FRAME, 0, WALL_ANIMATION_DELAY);
	base.addComponent<InputComponent>(*input);
	base.addComponent<HealthComponent>(100, 100, graphics);

}
void TowerManager::createHorizontalWall(VECTOR2 position)
{
	auto& wall(manager->addEntity());
	wall.addGroup(TwoTowers::walls);
	wall.addComponent<TransformComponent>(position.x, position.y, HORIZONTAL_WALL_WIDTH,HORIZONTAL_WALL_HEIGHT, 1.5);
	wall.addComponent<SpriteComponent>(graphics, HORIZONTAL_WALL_IMAGE, HORIZONTAL_WALL_WIDTH, HORIZONTAL_WALL_HEIGHT, HORIZONTAL_WALL_COLS);
}
void TowerManager::createVerticalWall(VECTOR2 position)
{
	auto& wall(manager->addEntity());
	wall.addGroup(TwoTowers::walls);
	wall.addComponent<TransformComponent>(position.x, position.y, VERTICAL_WALL_WIDTH,VERTICAL_WALL_HEIGHT, 1.5);
	wall.addComponent<SpriteComponent>(graphics, VERTICAL_WALL_IMAGE, VERTICAL_WALL_WIDTH, VERTICAL_WALL_HEIGHT, VERTICAL_WALL_COLS);
}

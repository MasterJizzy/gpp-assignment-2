#pragma once
#include <string>
#include "TextureManager.h"
#include "graphics.h"
#include "game.h"
#include "ECS.h"
#include "ProjectileManager.h"

class TowerManager
{
private:
	Graphics* graphics;
	Manager* manager;
	Input* input;
	ProjectileManager* pmanager;
public:
	TowerManager(Manager* man, Graphics* gra, Input* in, ProjectileManager* pmanager);
	~TowerManager();
	//friendly
	void createFriendlyTurret(VECTOR2 postiion);
	void createFriendlyWall(VECTOR2 postiion);
	void createFriendlyBase(VECTOR2 postiion);
	void createEnemyTurret(VECTOR2 postiion);
	void createEnemyWall(VECTOR2 postiion);
	void createEnemyBase(VECTOR2 postiion);
	void createHorizontalWall(VECTOR2 position);
	void createVerticalWall(VECTOR2 position);
};



#pragma once
#include "textDX.h"
#include "Components.h"
#include <string>


class HealthComponent : public Component {
private:
	float max_health;
	float current_health;
	TransformComponent* transform_;
	TextDX* dxFontSmall;       // DirectX fonts
	TextDX* dxFontMedium;
	TextDX* dxFontLarge;
	Graphics* graphics;
	bool initialized_ = false;


public:
	HealthComponent(float mh,float ch, Graphics* g)
		:
		graphics(g)
	{
		max_health = mh;
		current_health = ch;
	}

	~HealthComponent()
	{

	}

	void init() override
	{
		//what happens when this component is created
		max_health = 100;
		current_health = 100;
		/*transform_ = &entity->getComponent<TransformComponent>();*/
		initialized_ = true;
		dxFontSmall = new TextDX();     // DirectX fonts
		if (dxFontSmall->initialize(graphics, 15, true, false, "Arial") == false)
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));
	}

	void update(const float& frametime) override
	{
		
		//what happens every tick

	}
	int getcurrenthealth()
	{

		return current_health;
		/*return current_health; */
	}

	int changecurrenthealth(int minusby)
	{
		current_health -= minusby;
		return current_health;
	}
	void draw() override
	{
		//draw health text
		int icurrent_health = current_health;
		dxFontSmall->print(std::to_string(icurrent_health), entity->getComponent<TransformComponent>().position.x - entity->getComponent<SpriteComponent>().GetWidth()/2, entity->getComponent<TransformComponent>().position.y - entity->getComponent<SpriteComponent>().GetHeight() / 2);
	}
	float getHealthPercentage()
	{
		return current_health / max_health * 100;
	}
};


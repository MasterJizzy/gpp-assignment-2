#include "NodeGraph.h"
#include "twotowers.h"
#include <vector>

NodeGraph::NodeGraph(std::string filename, TowerManager* t)
	:
	towermanager(t),
	connection_array_(),
	node_array_()
{
	std::ifstream myReadFile(filename);
	column_units = 0;
	row_units = 0;
	std::string STRING;
	string content;
	char c;
	if (myReadFile.is_open())
	{
		while (getline(myReadFile, STRING))
		{
			content += STRING;
			row_units++;
			column_units = 0;
			for (int i = 0; i < STRING.length(); i++)
			{
				if (max_nodes_ > current_nodes_)
				{
					column_units++;
					node_array_[current_nodes_] = new Node(VECTOR2(0, 0));
					current_nodes_++;
				}
			}
		}
	}
	myReadFile.close();
	int column_count = 0;
	int row_count = 0;
	for (int i = 0; i < current_nodes_; i++)
	{
		if (column_count < column_units)
		{
			column_count++;
			node_array_[i]->position_ = VECTOR2(((GAME_WIDTH / (column_units+1))) * column_count, ((GAME_HEIGHT / (row_units))) * row_count);


		}
		else
		{
			column_count = 1;
			row_count++;
			node_array_[i]->position_ = VECTOR2(((GAME_WIDTH / (column_units+1))) * column_count, ((GAME_HEIGHT / (row_units))) * row_count);
		}
	}
		int i = -1;
		for each (char c in content)
		{
			i++;
			c = content[i];
			if (c == 'x')
			{
				towermanager->createFriendlyWall(node_array_[i]->position_);
				node_array_[i]->changeActive(false);
			}
			else if (c == 'f')
			{
				towermanager->createFriendlyBase(node_array_[i]->position_);
				node_array_[i]->changeActive(false);
			}
			else if (c == 'e')
			{
				towermanager->createEnemyBase(node_array_[i]->position_);
				node_array_[i]->changeActive(false);
			}
		}
	for (int i = 0; i < current_nodes_; i++)
	{
		for (int p = 0; p < current_nodes_; p++)
		{
			if (node_array_[i]->position_ + VECTOR2((GAME_WIDTH / (column_units+1)),0) == node_array_[p]->position_)
			{
				addConnection(5.0f, node_array_[i], node_array_[p]);
				node_array_[i]->addConnection(node_array_[p],5.0f);
			}
			if (node_array_[i]->position_ - VECTOR2((GAME_WIDTH / (column_units + 1)), 0) == node_array_[p]->position_)
			{
				addConnection(5.0f, node_array_[i], node_array_[p]);
				node_array_[i]->addConnection(node_array_[p], 5.0f);
			}
			if (node_array_[i]->position_ + VECTOR2(0, (GAME_HEIGHT / row_units)) == node_array_[p]->position_)
			{
				addConnection(5.0f, node_array_[i], node_array_[p]);
				node_array_[i]->addConnection(node_array_[p], 5.0f);
			}
			if (node_array_[i]->position_ - VECTOR2(0, (GAME_HEIGHT / row_units)) == node_array_[p]->position_)
			{
				addConnection(5.0f, node_array_[i], node_array_[p]);
				node_array_[i]->addConnection(node_array_[p], 5.0f);
			}
		}

	}
}
NodeGraph::~NodeGraph()
{

}
void NodeGraph::drawGraph(Graphics* graphics, Manager* manager)
{
	Node* node;
	for (int i = 0; i < current_nodes_; i++)
	{
		node = node_array_[i];
		auto& tile(manager->addEntity());
		tile.addComponent<TransformComponent>(node->position_.x, node->position_.y, TILE_WIDTH, TILE_HEIGHT, 1.0);
		tile.addComponent<SpriteComponent>(graphics, TILE_IMAGE, TILE_WIDTH, TILE_HEIGHT, TILE_COLS);
		tile.getComponent<SpriteComponent>().SetCurrentFrame(0);
		tile.getComponent<TransformComponent>().updatePosition(tile.getComponent<SpriteComponent>().getCenterX(), tile.getComponent<SpriteComponent>().getCenterY());
		tile.addGroup(TwoTowers::tiles);
	}
}
void NodeGraph::addConnection(float cost, Node* f, Node* t)
{
	if (current_connections_ < max_connections_)
	{
		connection_array_[current_connections_] = new Connection(cost, f, t);
		current_connections_++;
	}
}
bool NodeGraph::hasConnection(Node* f, Node* t)
{
	for (int i =0;i<current_connections_;i++)
	{
		if (connection_array_[i]->fromNode == f && connection_array_[i]->toNode == t)
		{
			return true;
			break;
		}
		else
		{
			return false;
		}
	}
}
Node* NodeGraph::getNearestNode(VECTOR2 pos)
{
	Node* nearest_node = nullptr;
	float nearest_distance = 0;
	for (int i = 0; i < current_nodes_; i++)
	{
		if (nearest_node == nullptr)
		{
			nearest_node = node_array_[i];
			nearest_distance = Magnitude(pos - nearest_node->position_);
		}
		else
		{
			if (Magnitude(pos - node_array_[i]->position_)<nearest_distance)
			{
				nearest_node = node_array_[i];
				nearest_distance = Magnitude(pos - node_array_[i]->position_);
			}
		}
	}
	return nearest_node;
}
//magnitude calculations
float NodeGraph::Magnitude(VECTOR2 v)
{
	return sqrt(v.x * v.x + v.y * v.y);
}
VECTOR2 NodeGraph::Normalized(VECTOR2 v)
{
	if (Magnitude(v) == 0.0f) {
		return { 0.0f, 0.0f };
	}
	return (v / Magnitude(v));
}
float NodeGraph::MagnitudeSq(VECTOR2 v)
{
	return v.x * v.x + v.y * v.y;
}
void NodeGraph::selectionSort(NodeRecord* a[], int n) {
	int i, j, min;
	NodeRecord* temp;
	for (i = 0; i <  - 1; i++) {
		min = i;
		for (j = i + 1; j < n; j++)
			if (a[j]->estimatedCost < a[min]->estimatedCost)
				min = j;
		temp = a[i];
		a[i] = a[min];
		a[min] = temp;
	}
}
bool NodeGraph::nodeRecordFound(NodeRecord* nodelist[], Node* nr, int n)
{
	bool found = false;
	for (int i = 0; i < n; i++) 
	{
		if (nodelist[i]->node == nr) {
			found == true;
			break;
		}
	}
	return found;
}
void NodeGraph::reverseArray(Connection* arr[], int start, int end)
{
	while (start < end)
	{
		Connection* temp = arr[start];
		arr[start] = arr[end];
		arr[end] = temp;
		start++;
		end--;
	}
}
void NodeGraph::drawWalls()
{
	for (int i = 0; i < current_nodes_; i++)
	{
		for (int p = 0; p < current_nodes_; p++)
		{
			if (node_array_[i]->position_ + VECTOR2((GAME_WIDTH / (column_units+1)), 0) == node_array_[p]->position_)
			{
				if (node_array_[i]->active == false && node_array_[p]->active == false && node_array_[p]->have_horizontal_wall_==false)
				{
					towermanager->createHorizontalWall(node_array_[i]->position_ + VECTOR2(WALL_WIDTH / 2, HORIZONTAL_WALL_HEIGHT/4));
					node_array_[p]->have_horizontal_wall_ = true;
				}
			}
			if (node_array_[i]->position_ + VECTOR2(0, (GAME_HEIGHT / row_units)) == node_array_[p]->position_)
			{
				if (node_array_[i]->active == false && node_array_[p]->active == false && node_array_[p]->have_vertical_wall_ == false)
				{
					towermanager->createVerticalWall(node_array_[i]->position_ + VECTOR2(VERTICAL_WALL_WIDTH/4, WALL_HEIGHT / 2));
					node_array_[p]->have_vertical_wall_ = true;
				}

			}
		}

	}
}
void NodeGraph::pathFindAStar(Node* start, Node* end)
{/*
	vector<NodeRecord>openList;
	vector<NodeRecord>closeList;
	openList.push_back(NodeRecord(start,connection_array_[0],0.0f));
	NodeRecord current = openList[0];
	while (!openList.empty())
	{
		current = openList[0];
		//OutputDebugString((to_string(current.id) + "\n").c_str());
		int toBeDeletedIndex = 0;
		for (int i = 0; i < openList.size(); i++)
		{
			if (current.estimatedCost > openList[i].estimatedCost)
			{
				//OutputDebugString((to_string(i) + "\n").c_str());				
				current = openList[i];
				toBeDeletedIndex = i;
			}
		}
		openList.erase(openList.begin() + toBeDeletedIndex);
		if (current.node == end)
		{
			break;
		}
		for (int c = 0; c < current_connections_; c++)
		{
			Node endNode = *connection_array_[c]->toNode;
			float endNodeCost = current.costSoFar + connection_array_[c]->cost_;
			bool isNotInList = true;
			bool isNotFound = true;
			for (int i = 0; i < closeList.size(); i++)
			{
				if (closeList[i].node == &endNode)
				{
					isNotFound == false;
					break;
				}
			}
			if (isNotFound==false)
			{
				continue;
			}
			else
			{
				for (int i = 0; i < openList.size(); i++)
				{
					if (openList[i].node == &endNode)
					{
						if (openList[i].costSoFar > endNodeCost)
						{
							openList[i].costSoFar = endNodeCost;
							openList[i].con = connection_array_[c];
							openList[i].estimatedCost = endNodeCost + Magnitude(end->position_ - endNode.position_);
							isNotInList = false;
						}
					}
				}
			}
			if (isNotInList && isNotFound)
			{
				openList.push_back(NodeRecord(&endNode, connection_array_[c], endNodeCost, endNodeCost + Magnitude(end->position_ - endNode.position_)));
			}

		}
		closeList.push_back(current);
	}
	
	if(current.node == end)
	{
		while (current.node != start)
		{
			if (current_path_conn_ < max_path_conn_)
			{
				path[current_path_conn_] = current.con;
				current_path_conn_++;
				for (int i = 0; i < closeList.size(); i++)
				{
					if (closeList[i].node == current.con->fromNode)
					{
						current = closeList[i];
						break;
					}
				}
			}
		}
		reverseArray(path,0,current_path_conn_);
	}*/
	//TEMPORARY SOLUTION
	if (current_path_conn_ < max_path_conn_)
	{
		path[current_path_conn_] = start->connection_array_[1];
		current_path_conn_++;
	}
	if (current_path_conn_ < max_path_conn_)
	{
		path[current_path_conn_] = path[current_path_conn_-1]->toNode->connection_array_[2];
		current_path_conn_++;
	}
	if (current_path_conn_ < max_path_conn_)
	{
		path[current_path_conn_] = path[current_path_conn_-1]->toNode->connection_array_[2];
		current_path_conn_++;
	}
	for (int i = 0; i < current_path_conn_; i++)
	{
		OutputDebugString((to_string(path[i]->toNode->position_.y)).c_str());
	}
}

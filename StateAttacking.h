#pragma once
#include "Components.h"
#include "State.h"
#include "twotowers.h"
#include "ProjectileManager.h"

class State;
class StateAttacking : public State
{
private:
	Entity *target;
	Manager *manager;
	Graphics* graphics;
	ProjectileManager* pmanager;
public:
	StateAttacking(Entity* o, std::string n, Manager* m,  ProjectileManager* p)
		:
		State(o, n),
		manager(m),
		pmanager(p)
		
	{

	}
	~StateAttacking()
	{

	}
	void init() override
	{
		/*
		//add sprite change here
		if (owner_->hasGroup(TwoTowers::enemy))
		{
			if (owner_->hasGroup(TwoTowers::ranged))
			{
				if (owner_->hasGroup(TwoTowers::units))
				{
					//if ranged enemy unit
					owner_->getComponent<SpriteComponent>().InitializeAnimation(12, 15, 0.2f);
				}
				else if (owner_->hasGroup(TwoTowers::towers))
				{
					//if ranged enemy tower
					owner_->getComponent<SpriteComponent>().InitializeAnimation(1, 6, 1.0f);
				}
			}
			else if (owner_->hasGroup(TwoTowers::melee))
			{
				if (owner_->hasGroup(TwoTowers::units))
				{
					//if melee enemy unit
					owner_->getComponent<SpriteComponent>().InitializeAnimation(26, 27, 0.2f);
				}
				else if (owner_->hasGroup(TwoTowers::towers))
				{
					//if enemy wall
				}
			}
			else if (owner_->hasGroup(TwoTowers::base_tower))
			{
				//if enemy base

			}
		}
		else if (owner_->hasGroup(TwoTowers::friendly))
		{
			if (owner_->hasGroup(TwoTowers::ranged))
			{
				if (owner_->hasGroup(TwoTowers::units))
				{
					//if ranged friendly unit
					owner_->getComponent<SpriteComponent>().InitializeAnimation(4, 7, 0.2f);
				}
				else if (owner_->hasGroup(TwoTowers::towers))
				{
					//if ranged friendly tower
					owner_->getComponent<SpriteComponent>().InitializeAnimation(1, 6, 1.0f);
				}
			}
			else if (owner_->hasGroup(TwoTowers::melee))
			{
				if (owner_->hasGroup(TwoTowers::units))
				{
					//if melee friendly unit
					owner_->getComponent<SpriteComponent>().InitializeAnimation(18, 19, 0.2f);
				}
				else if (owner_->hasGroup(TwoTowers::towers))
				{
					//if friendly wall

				}
			}
			else if (owner_->hasGroup(TwoTowers::base_tower))
			{
				//if friendly base

			}
		}*/
	}
	void update() override
	{
		OutputDebugString((name+"\n").c_str());
		isEnemyNearby(10);
		if (owner_->hasGroup(TwoTowers::units))
		{
			if (owner_->hasGroup(TwoTowers::melee))
			{
				owner_->getComponent<PhysicsComponent>().SetVelocity(target->getComponent<TransformComponent>().position);
				MeleeAttackEnemy(target);
			}
		}
		if (owner_->hasGroup(TwoTowers::ranged))
		{
			pmanager->CreateProjectile(owner_->getComponent<TransformComponent>().center_position, target->getComponent<TransformComponent>().center_position/3,owner_);
			if (pmanager->CheckProjectile(target))
			{
				RangeAttackEnemy(target);
			}
		}
	}
	void check() override
	{
		if (!isEnemyNearby(10))
		{
			owner_->getComponent<StateComponent>().changeState("seeking");
		}
		if (owner_->hasGroup(TwoTowers::units))
		{
			if (owner_->getComponent<HealthComponent>().getHealthPercentage() <= 0)
			{
				owner_->getComponent<StateComponent>().changeState("dying");
			}
		}

	}
	bool isEnemyNearby(int range)
	{
		if (owner_->hasGroup(TwoTowers::friendly))
		{
			auto& entityarr(manager->getGroup(TwoTowers::enemy));
			bool isTrue = false;
			//check if enemy is nearby
			//also neeeded in StateAttacking
			for (auto& e : entityarr)
			{
				if ((e->getComponent<TransformComponent>().position.x > (owner_->getComponent<TransformComponent>().position.x - range)) &&
					(e->getComponent<TransformComponent>().position.x < (owner_->getComponent<TransformComponent>().position.x + range)) &&
					(e->getComponent<TransformComponent>().position.y > (owner_->getComponent<TransformComponent>().position.y - range)) &&
					(e->getComponent<TransformComponent>().position.y < (owner_->getComponent<TransformComponent>().position.y + range)))
				{

					target = e;
					isTrue = true;
					break;
				}
			}
			return isTrue;
		}
		else if (owner_->hasGroup(TwoTowers::enemy))
		{
			auto& entityarr(manager->getGroup(TwoTowers::friendly));
			bool isTrue = false;
			//check if enemy is nearby
			//also neeeded in StateAttacking
			for (auto& e : entityarr)
			{
				if ((e->getComponent<TransformComponent>().position.x > (owner_->getComponent<TransformComponent>().position.x - range)) &&
					(e->getComponent<TransformComponent>().position.x < (owner_->getComponent<TransformComponent>().position.x + range)) &&
					(e->getComponent<TransformComponent>().position.y > (owner_->getComponent<TransformComponent>().position.y - range)) &&
					(e->getComponent<TransformComponent>().position.y < (owner_->getComponent<TransformComponent>().position.y + range)))
				{
					target = e;
					isTrue = true;
					break;
				}
			}
			return isTrue;
		}
		return false;
	}


	void MeleeAttackEnemy(Entity* enemy)
	{
		//attack enemy (for melee units)
		if (target->hasGroup(TwoTowers::units) || target->hasGroup(TwoTowers::base_tower))
		{
			target->getComponent<HealthComponent>().changecurrenthealth(10);
			PlaySound("sounds\\music4.wav", 0, SND_FILENAME | SND_ASYNC);
		}


	}
	void RangeAttackEnemy(Entity* enemy)
	{
		//attack enemy (for ranged units)
		if (target->hasGroup(TwoTowers::units) || target->hasGroup(TwoTowers::base_tower))
		{
			target->getComponent<HealthComponent>().changecurrenthealth(10);
			PlaySound("sounds\\music2.wav", 0, SND_FILENAME | SND_ASYNC);
		}
	}

};

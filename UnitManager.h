#pragma once
#include <string>
#include "TextureManager.h"
#include "graphics.h"
#include "game.h"
#include "ECS.h"
#include "ProjectileManager.h"
#include "NodeGraph.h"

class UnitManager
{
public:
	UnitManager(Manager* man, Graphics* gra, Input* in, ProjectileManager* pmanager,NodeGraph* n);
	~UnitManager();
	//friendly
	void createFriendlyMarine(VECTOR2 postiion);
	void createFriendlyKnight(VECTOR2 postiion);
	void createEnemyMarine(VECTOR2 postiion);
	void createEnemyKnight(VECTOR2 postiion);
private:
	Graphics* graphics;
	Manager* manager;
	Input* input;
	ProjectileManager* pmanager;
	NodeGraph* nodegraph;
};

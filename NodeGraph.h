#pragma once
#include "Node.h"
#include <iostream>
#include <fstream>
#include <string> 
#include "graphics.h"
#include "TowerManager.h"
#include "ECS.h"

class NodeGraph
{
private:
	TowerManager* towermanager;
	int column_units;
	int row_units;
public:
	Connection* connection_array_[1000];
	int current_connections_ = 0;
	int max_connections_ = 1000;
	Node* node_array_[150];
	int current_nodes_ = 0;
	int max_nodes_ = 150;
	Connection* path[100];
	int current_path_conn_ = 0;
	int max_path_conn_ = 100;
	Connection* enemy_path[100];
	int current_enemy_path_conn_ = 0;
	int max_enemy_path_conn_ = 100;
	NodeGraph(std::string file, TowerManager* t);
	~NodeGraph();
	void addConnection(float cost, Node* f, Node* t);
	bool hasConnection(Node* f, Node* t);
	Node* getNearestNode(VECTOR2 pos);
	float Magnitude(VECTOR2 v);
	VECTOR2 Normalized(VECTOR2 v);
	float MagnitudeSq(VECTOR2 v);
	void drawGraph(Graphics* graphics, Manager* manager);
	void selectionSort(NodeRecord* a[], int n);
	bool nodeRecordFound(NodeRecord* nl[], Node* nr, int n);
	void reverseArray(Connection* arr[], int start, int end);
	void drawWalls();
	void pathFindAStar(Node* start, Node* end);
};


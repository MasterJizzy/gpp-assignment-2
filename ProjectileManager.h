#pragma once

#include <map>
#include <string>
#include "TextureManager.h"
#include "graphics.h"
#include "game.h"
#include "ECS.h"


class ProjectileManager
{
public:
	ProjectileManager(Manager* man, Graphics* gra);
	~ProjectileManager();

	//gameobjects

	void CreateProjectile(VECTOR2 pos, VECTOR2 vel, Entity* owner);
	bool CheckProjectile(Entity* target);


private:
	Graphics* graphics;
	Manager* manager;
};


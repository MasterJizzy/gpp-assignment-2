#pragma once
#include "Components.h"
#include "input.h"
#include <Windows.h>
#include <mmsystem.h>
#include <string>
#include <sstream>

class InputComponent : public Component {
private:
	Input& input_;
	bool input_flag;
	VECTOR2 mousePosition;
	std::stringstream ss;
public:

	InputComponent(Input& input)
		:
		input_(input)
	{
	}

	~InputComponent()
	{
	}

	void init() override
	{

	}

	void update(const float& frametime) override
	{
		
	}

	void draw() override
	{

	}
	VECTOR2 getMousePosition()
	{
		mousePosition = VECTOR2(input_.getMouseX(), input_.getMouseY());
		return mousePosition;
	}
	bool getInputFlag()
	{
		return input_flag;

	}
	bool setInputFlag(bool inputflag)
	{

		input_flag = inputflag;
		return input_flag;
	}
	bool isClickedOn()
	{
		input_flag = false;
		if ((input_.getMouseX() > (entity->getComponent<TransformComponent>().position.x)) &&
			(input_.getMouseX() < (entity->getComponent<TransformComponent>().position.x + (entity->getComponent<SpriteComponent>().GetWidth()) * entity->getComponent<TransformComponent>().scale)) &&
			(input_.getMouseY() > (entity->getComponent<TransformComponent>().position.y)) &&
			(input_.getMouseY() < (entity->getComponent<TransformComponent>().position.y + (entity->getComponent<SpriteComponent>().GetHeight()) * entity->getComponent<TransformComponent>().scale)))
		{
			if (input_.getMouseLButton())
			{
				getMousePosition();
				Sleep(100);
				input_flag = true;
				return true;
			}
			else 
			{
				return true;
			}
		}
		else
		{
			return false;
		}
	}
};


#ifndef _CONSTANTS_H            // prevent multiple definitions if this 
#define _CONSTANTS_H            // ..file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include <windows.h>


//-----------------------------------------------
// Useful macros
//-----------------------------------------------
// Safely delete pointer referenced item
#define SAFE_DELETE(ptr)       { if (ptr) { delete (ptr); (ptr)=NULL; } }
// Safely release pointer referenced item
#define SAFE_RELEASE(ptr)      { if(ptr) { (ptr)->Release(); (ptr)=NULL; } }
// Safely delete pointer referenced array
#define SAFE_DELETE_ARRAY(ptr) { if(ptr) { delete [](ptr); (ptr)=NULL; } }
// Safely call onLostDevice
#define SAFE_ON_LOST_DEVICE(ptr)    { if(ptr) { ptr->onLostDevice(); } }
// Safely call onResetDevice
#define SAFE_ON_RESET_DEVICE(ptr)   { if(ptr) { ptr->onResetDevice(); } }
#define TRANSCOLOR  SETCOLOR_ARGB(0,255,0,255)  // transparent color (magenta)


//-----------------------------------------------
//                  Constants
//-----------------------------------------------
// game
const char CLASS_NAME[] = "Spacewar";
const char GAME_TITLE[] = "Spacewar";
const bool FULLSCREEN = true;              // windowed or fullscreen
const UINT GAME_WIDTH =  1920;               // width of game in pixels
const UINT GAME_HEIGHT = 1080;               // height of game in pixels
// window
const UINT WIN_WIDTH = 1920/2;               // width of game in pixels
const UINT WIN_HEIGHT = 1080/2;               // height of game in pixels

// game
const double PI = 3.14159265;
const float FRAME_RATE  = 200.0f;               // the target frame rate (frames/sec)
const float MIN_FRAME_RATE = 10.0f;             // the minimum frame rate
const float MIN_FRAME_TIME = 1.0f/FRAME_RATE;   // minimum desired time for 1 frame
const float MAX_FRAME_TIME = 1.0f/MIN_FRAME_RATE; // maximum time used in calculations

//bat
const int  BAT_START_FRAME = 0;         // starting frame of ship animation
const int  BAT_END_FRAME = 4;           // last frame of ship animation
const float BAT_ANIMATION_DELAY = 0.2f; // time between frames of ship animation
const int  BAT_COLS = 5;                // ship texture has 2 columns
const int  BAT_WIDTH = 32;              // width of ship image
const int  BAT_HEIGHT = 32;             // height of ship image
const float BAT_SPEED = 100.0f;                // pixels per second
const float BAT_SCALE = 1.5f;                  // starting ship scale

//robot
const int  ROBOT_START_FRAME = 0;         // starting frame of ship animation
const int  ROBOT_END_FRAME = 1;           // last frame of ship animation
const float ROBOT_ANIMATION_DELAY = 0.2f; // time between frames of ship animation
const int  ROBOT_COLS = 4;                // ship texture has 2 columns
const int  ROBOT_WIDTH = 32;              // width of ship image
const int  ROBOT_HEIGHT = 32;             // height of ship image
const float ROBOT_SPEED = 100.0f;                // pixels per second
const float ROBOT_SCALE = 1.5f;                  // starting ship scale

//wall
const int  WALL_START_FRAME = 0;         // starting frame of ship animation
const int  WALL_END_FRAME = 0;           // last frame of ship animation
const float WALL_ANIMATION_DELAY = 0.2f; // time between frames of ship animation
const int  WALL_COLS = 1;                // ship texture has 2 columns
const int  WALL_WIDTH = 32;              // width of ship image
const int  WALL_HEIGHT = 32;             // height of ship image
const float WALL_SPEED = 100.0f;                // pixels per second
const float WALL_SCALE = 2.0f;                  // starting ship scale
//horizontal wall
const int  HORIZONTAL_WALL_START_FRAME = 0;         // starting frame of ship animation
const int  HORIZONTAL_WALL_END_FRAME = 0;           // last frame of ship animation
const int HORIZONTAL_WALL_COLS = 0;                // ship texture has 2 columns
const int  HORIZONTAL_WALL_WIDTH = 142;              // width of ship image
const int HORIZONTAL_WALL_HEIGHT = 32;             // height of ship image
const float HORIZONTAL_WALL_SCALE = 1.0f;                  // starting ship scale
//vertical wall
const int  VERTICAL_WALL_START_FRAME = 0;         // starting frame of ship animation
const int  VERTICAL_WALL_END_FRAME = 0;           // last frame of ship animation
const int   VERTICAL_WALL_COLS = 0;                // ship texture has 2 columns
const int  VERTICAL_WALL_WIDTH = 32;              // width of ship image
const int   VERTICAL_WALL_HEIGHT = 66;             // height of ship image
const float VERTICAL_WALL_SCALE = 1.0f;                  // starting ship scale

//allytower
const int  FRIENDLY_TOWER_START_FRAME = 0;         // starting frame of ship animation
const int  FRIENDLY_TOWER_END_FRAME = 6;           // last frame of ship animation
const float FRIENDLY_TOWER_ANIMATION_DELAY = 1.0f; // time between frames of ship animation
const int  FRIENDLY_TOWER_COLS = 7;                // ship texture has 2 columns
const int  FRIENDLY_TOWER_WIDTH = 32;              // width of ship image
const int  FRIENDLY_TOWER_HEIGHT = 32;             // height of ship image
const float FRIENDLY_TOWER_SPEED = 100.0f;                // pixels per second
const float FRIENDLY_TOWER_SCALE = 1.5f;                  // starting ship scale

//enemytower
const int  ENEMY_TOWER_START_FRAME = 0;         // starting frame of ship animation
const int  ENEMY_TOWER_END_FRAME = 6;           // last frame of ship animation
const float ENEMY_TOWER_ANIMATION_DELAY = 1.0f; // time between frames of ship animation
const int  ENEMY_TOWER_COLS = 7;                // ship texture has 2 columns
const int  ENEMY_TOWER_WIDTH = 32;              // width of ship image
const int  ENEMY_TOWER_HEIGHT = 32;             // height of ship image
const float ENEMY_TOWER_SPEED = 100.0f;                // pixels per second
const float ENEMY_TOWER_SCALE = 1.5f;                  // starting ship scale

//gamesprite
const int  GAMESPRITE_START_FRAME = 0;         // starting frame of ship animation
const int  GAMESPRITE_END_FRAME = 39;           // last frame of ship animation
const float GAMESPRITE_ANIMATION_DELAY = 0.2f; // time between frames of ship animation
const int  GAMESPRITE_COLS = 8;                // ship texture has 2 columns
const int  GAMESPRITE_WIDTH = 32;              // width of ship image
const int  GAMESPRITE_HEIGHT = 32;             // height of ship image
const float GAMESPRITE_SPEED = 100.0f;                // pixels per second
const float GAMESPRITE_SCALE = 3.0f;                  // starting ship scale

//healthbar
const int HEALTH_START_FRAME = 0;
const int HEALTH_END_FRAME = 4;
const float HEALTH_ANIMATION_DELAY = 100.0f;
const int	HEALTH_COLS= 5;
const int	HEALTH_WIDTH = 64;
const int   HEALTH_HEIGHT = 15;


//Menu Background
const int MENUBACK_COLS = 0;
const int MENUBACK_WIDTH = 1280;
const int MENUBACK_HEIGHT = 720;

//Menu Play Button
const int MENU_PLAY_START_FRAME = 0;
const int MENU_PLAY_END_FRAME = 1;
const float MENU_PLAY_ANIMATION_DELAY = 10000000.0f;
const int MENU_PLAY_COLS = 2;
const int MENU_PLAY_WIDTH = 1019;
const int MENU_PLAY_HEIGHT = 337;

//Menu Exit Button
const int MENU_EXIT_START_FRAME = 2;
const int MENU_EXIT_END_FRAME = 3;
const float MENU_EXIT_ANIMATION_DELAY = 10000000.0f;
const int MENU_EXIT_COLS = 2;
const int MENU_EXIT_WIDTH = 1019;
const int MENU_EXIT_HEIGHT = 337;

//Menu Exit Button
const int MENU_LEVEL_START_FRAME = 4;
const int MENU_LEVEL_END_FRAME = 5;
const float MENU_LEVEL_ANIMATION_DELAY = 10000000.0f;
const int MENU_LEVEL_COLS = 2;
const int MENU_LEVEL_WIDTH = 1019;
const int MENU_LEVEL_HEIGHT = 337;

//Game Background
const int GAMEBACK_COLS = 0;
const int GAMEBACK_WIDTH = 626;
const int GAMEBACK_HEIGHT = 417;

//Cursor
const int CURSOR_COLS = 0;
const int CURSOR_WIDTH = 131;
const int CURSOR_HEIGHT = 140;

//Tower 1
const int GUI_TOWER_START_FRAME = 2;
const int GUI_TOWER_END_FRAME = 3;
const int GUI_TOWER_COLS = 2;
const int GUI_TOWER_WIDTH = 32;
const int GUI_TOWER_HEIGHT = 32;

//Tower 2
const int GUI_TOWER_START_FRAME_2 = 0;
const int GUI_TOWER_END_FRAME_2 = 1;

//Tower Price
const int TOWER1_PRICE = 50;
const int TOWER2_PRICE = 100;

//Unit 1
const int GUI_UNIT_START_FRAME = 0;
const int GUI_UNIT_END_FRAME = 1;
const int GUI_UNIT_COLS = 2;
const int GUI_UNIT_WIDTH = 32;
const int GUI_UNIT_HEIGHT = 32;

//Unit 2
const int GUI_UNIT_START_FRAME_2 = 2;
const int GUI_UNIT_END_FRAME_2 = 3;

//Unit Price
const int UNIT1_PRICE = 50;
const int UNIT2_PRICE = 100;

//Level1 Select
const int LEVEL1_START_FRAME = 0;
const int LEVEL1_END_FRAME = 1;
const float LEVEL1_ANIMATION_DELAY = 10000000.0f;
const int LEVEL1_COLS = 2;
const int LEVEL1_WIDTH = 1019;
const int LEVEL1_HEIGHT = 337;

//Level 2 Select
const int LEVEL2_START_FRAME = 2;
const int LEVEL2_END_FRAME = 3;
const float LEVEL2_ANIMATION_DELAY = 10000000.0f;
const int LEVEL2_COLS = 2;
const int LEVEL2_WIDTH = 1019;
const int LEVEL2_HEIGHT = 337;

//Back Select
const int LEVEL_BACK_START_FRAME = 4;
const int LEVEL_BACK_END_FRAME = 5;
const float LEVEL_BACK_ANIMATION_DELAY = 10000000.0f;
const int LEVEL_BACK_COLS = 2;
const int LEVEL_BACK_WIDTH = 1019;
const int LEVEL_BACK_HEIGHT = 337;

//Tile 
const int TILE_START_FRAME = 0;
const int TILE_END_FRAME = 3;
const int TILE_COLS = 4;
const int TILE_WIDTH = 32;
const int TILE_HEIGHT = 32;

// In this game simple constants are used for key mappings. If variables were used
// it would be possible to save and restore key mappings from a data file.
const UCHAR ESC_KEY      = VK_ESCAPE;   // escape key
const UCHAR ALT_KEY      = VK_MENU;     // Alt key
const UCHAR ENTER_KEY    = VK_RETURN;   // Enter key
const UCHAR MOUSE_POINT = VK_LBUTTON;

// graphic images

//Menu Related images
const char MENUBACK_IMAGE[] = "pictures\\Space background.jpg";
const char MENU_IMAGE[] = "pictures\\MainMenu.png";

//Level select Related images
const char LEVEL_SELECT_IMAGE[] = "pictures\\Level_select.png";

//Game Related images
const char GAMEBACK_IMAGE[] = "pictures\\sky background.jpg";
const char LEVEL_ONE_BG_IMAGE[] = "pictures\\Levels\\steel_background.jpg";
const char CURSOR_IMAGE[] = "pictures\\Cursor.png";

//GUI Related images
const char GUI_TOWER_IMAGE[] = "pictures\\Tower pic.png";
const char GUI_UNIT_IMAGE[] = "pictures\\Ally Unit.png";

const char FRIENDLY_BASE_IMAGE[] = "pictures\\Player_Base.png";
const char FRIENDLY_TOWER_IMAGE[] = "pictures\\friendly_turret.png";
const char FRIENDLY_TOWER_PROJECTILE_IMAGE[] = "pictures\\friendly_turrent_projectile.png";
const char BAT_IMAGE[] = "pictures\\bat.png";
const char ROBOT_IMAGE[] = "pictures\\robot.png";
const char ENEMY_BASE_IMAGE[] = "pictures\\Enemy_Base.png";
const char ENEMY_TOWER_IMAGE[] = "pictures\\enemy_turret.png";
const char ENEMYBAT_IMAGE[] = "pictures\\enemybat.png";
const char ENEMYROBOT_IMAGE[] = "pictures\\enemyrobot.png";
const char WALL_IMAGE[] = "pictures\\wall.png";
const char HORIZONTAL_WALL_IMAGE[] = "pictures\\horizontal_wall.png";
const char  VERTICAL_WALL_IMAGE[] = "pictures\\vertical_wall.png";
const char TILE_IMAGE[] = "pictures\\grid_sprite.png";
const char GAMESPRITE_IMAGE[] = "pictures\\gamesprite.png";

#endif

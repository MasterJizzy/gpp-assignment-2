#pragma once
#include "Components.h"


class CollisionComponent : public Component {
private:

public:
	CollisionComponent()
	{

	}
	~CollisionComponent() {
	}
	void init() override
	{

	}

	void update(const float& frametime) override
	{

	}

	void draw() override
	{

	}

	bool checkCollision(Entity* target_) 
	{
		if ((entity->getComponent<TransformComponent>().position.x > target_->getComponent<TransformComponent>().position.x) &&
			(entity->getComponent<TransformComponent>().position.x < target_->getComponent<TransformComponent>().position.x + target_->getComponent<SpriteComponent>().GetWidth()) &&
			(entity->getComponent<TransformComponent>().position.y > target_->getComponent<TransformComponent>().position.y) &&
			(entity->getComponent<TransformComponent>().position.y < target_->getComponent<TransformComponent>().position.y + target_->getComponent<SpriteComponent>().GetHeight()))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
};
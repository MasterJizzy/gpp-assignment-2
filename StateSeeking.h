#pragma once
#include "Components.h"
#include "State.h"
#include "Node.h"
#include "NodeGraph.h"
#include "twotowers.h"


class StateSeeking : public State
{

private:
	Node *target;
	Manager *manager;
	Connection* path[100];
	int current_nodes = 0;
	int max_nodes = 100;
	int currentNodeIndex = 0;
	Node* destination_node;
	NodeGraph* nodegraph;
public:
	StateSeeking(Entity* o, std::string n, Manager* m, NodeGraph* nodeg, VECTOR2 d)
		:
		State(o,n),
		manager(m),
		nodegraph(nodeg)
	{
		destination_node = nodegraph->getNearestNode(d);
	}
	StateSeeking(Entity* o, std::string n, Manager* m)
		:
		State(o, n),
		manager(m)
	{
	}
	~StateSeeking()
	{

	}
	void init() override
	{
		if (owner_->hasGroup(TwoTowers::units))
		{
			currentNodeIndex = 0;
			Node *current_node = nodegraph->getNearestNode(owner_->getComponent<TransformComponent>().center_position);
			//nodegraph->pathFindAStar(current_node, destination_node);
			for (current_nodes = 0; current_nodes < nodegraph->current_path_conn_; current_nodes++)
			{
				path[current_nodes] = nodegraph->path[current_nodes];
				OutputDebugString(((to_string(path[current_nodes]->toNode->id)+"\n").c_str()));
				current_nodes++;
			}
		}
		//add sprite change here
		if (owner_->hasGroup(TwoTowers::enemy))
		{
			if (owner_->hasGroup(TwoTowers::ranged))
			{
				if (owner_->hasGroup(TwoTowers::units))
				{
					//if ranged enemy unit
					owner_->getComponent<SpriteComponent>().InitializeAnimation(8, 11, 0.2f);
				}
				else if (owner_->hasGroup(TwoTowers::towers))
				{
					//if ranged enemy tower
					owner_->getComponent<SpriteComponent>().InitializeAnimation(0, 0, 1.0f);
				}
			}
			else if (owner_->hasGroup(TwoTowers::melee))
			{
				if (owner_->hasGroup(TwoTowers::units))
				{
					//if melee enemy unit
					owner_->getComponent<SpriteComponent>().InitializeAnimation(24, 25, 0.2f);
				}
				else if (owner_->hasGroup(TwoTowers::towers))
				{
					//if enemy wall
				}
			}
			else if (owner_->hasGroup(TwoTowers::base_tower))
			{
				//if enemy base
			}
		}
		else if (owner_->hasGroup(TwoTowers::friendly))
		{
			if (owner_->hasGroup(TwoTowers::ranged))
			{
				if (owner_->hasGroup(TwoTowers::units))
				{
					//if ranged friendly unit
					owner_->getComponent<SpriteComponent>().InitializeAnimation(0, 3, 0.2f);
				}
				else if (owner_->hasGroup(TwoTowers::towers))
				{
					//if ranged friendly tower
					owner_->getComponent<SpriteComponent>().InitializeAnimation(0, 0, 1.0f);
				}
			}
			else if (owner_->hasGroup(TwoTowers::melee))
			{
				if (owner_->hasGroup(TwoTowers::units))
				{
					//if melee friendly unit
					owner_->getComponent<SpriteComponent>().InitializeAnimation(16, 17, 0.2f);
				}
				else if (owner_->hasGroup(TwoTowers::towers))
				{
					//if friendly wall
					owner_->getComponent<SpriteComponent>().InitializeAnimation(0, 0, 0.2f);
				}
			}
			else if (owner_->hasGroup(TwoTowers::base_tower))
			{
				//if friendly base
				owner_->getComponent<SpriteComponent>().InitializeAnimation(0, 0, 0.2f);
			}
		}
	}

	void update() override
	{
		OutputDebugString((name + "\n").c_str());
		target = destination_node;
		if (owner_->hasGroup(TwoTowers::units))
		{
			/*for (int i = currentNodeIndex; i < current_nodes; i++)
			{
				if (owner_->getComponent<TransformComponent>().center_position == path[currentNodeIndex]->toNode->position_)
				{
					if (current_nodes > currentNodeIndex)
					{
						target = path[i + 1]->toNode;
						currentNodeIndex++;
					}
					break;
				}
				else
				{
					target = path[currentNodeIndex]->toNode;
				}
			}
			*/
			if (nodegraph->getNearestNode(owner_->getComponent<TransformComponent>().position) != destination_node)
			{
				owner_->getComponent<PhysicsComponent>().SetVelocity(VECTOR2(target->position_ / 4));
			}
			else
			{
				owner_->getComponent<PhysicsComponent>().SetVelocity(VECTOR2(0,0));

			}
		}
	}
	void check() override
	{

		if (isEnemyNearby(10))
		{
			owner_->getComponent<StateComponent>().changeState("attacking");
		}
		if (owner_->hasGroup(TwoTowers::units))
		{
			if (owner_->getComponent<HealthComponent>().getHealthPercentage() == 0)
			{
				owner_->getComponent<StateComponent>().changeState("dying");
			}
		}
	}
	bool isEnemyNearby(int range)
	{
		if (owner_->hasGroup(TwoTowers::friendly))
		{
			auto& entityarr(manager->getGroup(TwoTowers::enemy));
			bool isTrue = false;
			//check if enemy is nearby
			//also neeeded in StateAttacking
			for (auto& e : entityarr)
			{
				if ((e->getComponent<TransformComponent>().position.x > (owner_->getComponent<TransformComponent>().position.x - range)) &&
					(e->getComponent<TransformComponent>().position.x < (owner_->getComponent<TransformComponent>().position.x + range)) &&
					(e->getComponent<TransformComponent>().position.y > (owner_->getComponent<TransformComponent>().position.y - range)) &&
					(e->getComponent<TransformComponent>().position.y < (owner_->getComponent<TransformComponent>().position.y + range)))
				{
					isTrue = true;
					break;
				}
			}
			return isTrue;
		}
		else if (owner_->hasGroup(TwoTowers::enemy))
		{
			auto& entityarr(manager->getGroup(TwoTowers::friendly));
			bool isTrue = false;
			//check if enemy is nearby
			//also neeeded in StateAttacking
			for (auto& e : entityarr)
			{
				if ((e->getComponent<TransformComponent>().position.x > (owner_->getComponent<TransformComponent>().position.x - range)) &&
					(e->getComponent<TransformComponent>().position.x < (owner_->getComponent<TransformComponent>().position.x + range)) &&
					(e->getComponent<TransformComponent>().position.y > (owner_->getComponent<TransformComponent>().position.y - range)) &&
					(e->getComponent<TransformComponent>().position.y < (owner_->getComponent<TransformComponent>().position.y + range)))
				{
					isTrue = true;
					break;
				}
			}
			return isTrue;
		}
		return false;
	}
};

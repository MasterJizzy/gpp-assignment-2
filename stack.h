// Stack.h (Pointer-based implementation)
#pragma once
#include <iostream>
using namespace std;

class Stack
{
private:
	struct Node
	{
		string item;
		Node* next;
	};

	Node* topNode;

public:
	//Default constructor
	Stack();
	//Destructor
	~Stack();

	//check if the stack is empty
	bool isEmpty();

	//push item on top of the stack
	bool push(string& item);

	//pop item from top of stack
	bool pop();

	//retrieve and pop item from top of stack
	bool pop(string& item);

	//retrieve item from top of stack
	string getTop();
};


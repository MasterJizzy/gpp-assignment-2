#pragma once
#include <d3d9.h>
#include <d3dx9.h>
#include <string>

class Entity;
class State{
private:

public:
	Entity* owner_;
	std::string name;
	State() = default;
	State(Entity* o, std::string n)
	:
	owner_(o),
	name(n)
	{

	}
	virtual ~State()
	{}
	virtual void init()
	{}
	virtual void update()
	{}
	virtual void check()
	{}
};

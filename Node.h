#pragma once
#include "graphics.h"
class Connection;
class Node;

struct Connection
{
	//generating id
	int id;
	static int current_con_id;

	float cost_;
	Node *fromNode;
	Node *toNode;
	Connection(float c, Node* f, Node* t);
	~Connection();
};
struct Node
{
	//generating id
	int id;
	static int current_node_id;

	bool active;
	int current_connections_;
	int max_connections_;
	bool have_horizontal_wall_ = false;
	bool have_vertical_wall_ = false;
	VECTOR2 position_;
	Connection* connection_array_[10];
	void addConnection(Node* t, float c);
	void changeActive(bool a);
	Node(VECTOR2 p);
	~Node();
};
struct NodeRecord
{
	//generating id
	int id;
	static int current_node_record_id;

	Node* node;
	Connection* con;
	float costSoFar;
	float estimatedCost;
	NodeRecord(Node* n, Connection* c, float cost, float e = 0.0f);
	~NodeRecord();
};


